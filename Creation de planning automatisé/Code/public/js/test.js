console.log("In test.js");
/*
Fonction qui va être appelée afin de générer les evenements draggables.
Elle va vérifier le nombre de CM restants et les afficher si il en reste.
Il faut encore faire la vérification des TD restants pour chaque groupe donc
ajouter des méthodes dans la classe UE, pareil pour les TP.
*/

function loadExtEvents(liCodeUe, LiUE) {
  //Le format sera update a chaque changement de format via les checkbox de l'appli
  var format = sessionStorage["format"];
  //console.log("depuis test.js dans loadExtEvents ", format);

  for (var i in liCodeUe) {
    // CM
    var CMbloc = "";
    if (
      LiUE[i].getblocrestant("CM", LiUE[i].ListeGroupe[0]) > 0 &&
      LiUE[i].CM != undefined
    ) {
      CMbloc =
        '<li><a href="#">CM</a><ul>' +
        // CM SOUS MENUS
        "<ul>" +
        '<li><a href="#">' +
        //Creation du bloc CM debut
        //if (LiUE[i].getblocrestant("CM", "A") > 0 && LiUE[i].CM != undefined) {}
        '<div id="' +
        i +
        "\" class='fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event'>" +
        '<div class="fc-event-main">' +
        "<div>" +
        " CM : " +
        LiUE[i].getblocrestant("CM", LiUE[i].ListeGroupe[0]) +
        "/" +
        LiUE[i].getnbblocmax("CM") +
        //Le "A" au dessus est arbitraire car pour l'instant on considere qu'un CM concerne tout les groupes
        "</div>" +
        "</div>" +
        "</div>" +
        //Creation du bloc CM fin
        "</a></li>" +
        "</ul>";
      CMbloc += "</ul></li>";
      //console.log(CMbloc);
    }
    var TDbloc = "";
    var TDrepeat = "";
    var TPbloc = "";
    var TPrepeat = "";
    var close = "";
    //console.log(LiUE[i].ListeGroupe);
    LiUE[i].ListeGroupe.forEach((groupe) => {
      if (LiUE[i].getblocrestant("TD", groupe) > 0 && LiUE[i].TD != undefined) {
        // TDbloc = '<li><a href="#">TDe</a>';
        // TD SOUS MENUS
        //  "<ul>" +
        TDbloc = '<li><a href="#">TD</a><ul>';
        close = "</ul></li>";
        TDrepeat +=
          '<li><a href="#">' +
          //Contenu
          '<div id="' +
          i +
          "\" class='fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event'>" +
          '<div class="fc-event-main">' +
          "<div>" +
          " TD-" +
          groupe +
          " : " +
          LiUE[i].getblocrestant("TD", groupe) +
          "/" +
          LiUE[i].getnbblocmax("TD") +
          //Le "A" au dessus est arbitraire car pour l'instant on considere qu'un CM concerne tout les groupes
          "</div>" +
          "</div>" +
          "</div>" +
          //Fin contenu
          "</a></li>";
        // console.log("bloc :", TDbloc);
      }

      if (LiUE[i].getblocrestant("TP", groupe) > 0 && LiUE[i].TP != undefined) {
        // TDbloc = '<li><a href="#">TDe</a>';
        // TD SOUS MENUS
        //  "<ul>" +
        TPbloc = '<li><a href="#">TP</a><ul>';
        close = "</ul></li>";
        TPrepeat +=
          '<li><a href="#">' +
          //Contenu
          '<div id="' +
          i +
          "\" class='fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event'>" +
          '<div class="fc-event-main">' +
          "<div>" +
          " TP-" +
          groupe +
          " : " +
          LiUE[i].getblocrestant("TP", groupe) +
          "/" +
          LiUE[i].getnbblocmax("TP") +
          //Le "A" au dessus est arbitraire car pour l'instant on considere qu'un CM concerne tout les groupes
          "</div>" +
          "</div>" +
          "</div>" +
          //Fin contenu
          "</a></li>";
        //console.log("bloc :", TDbloc);
        // console.log("tp: ", TPbloc);
      }
    });

    TDbloc += TDrepeat + close;
    TPbloc += TPrepeat + close;

    document.getElementById("external-events").innerHTML +=
      '<ul id="menu-accordeon">' +
      "<p> <strong>" +
      "<div>" +
      LiUE[i].Nom +
      "</div> " +
      "<div>" +
      LiUE[i].CodeUE +
      "</div> </strong> </p>" +
      CMbloc +
      TDbloc +
      TPbloc +
      "</ul>";
  }
}

function reloadEvents(LiCodeUe, LiUE) {
  document.getElementById("external-events").innerHTML = "";
  loadExtEvents(LiCodeUe, LiUE);
}
/*
Fonction qui va récupérer vérifier si l'élément qu'on drop est un CM, un TD ou un TP.
On s'en sert dans la méthode drop du calendrier dans test5.html.
*/
/*function course(str_course) {
  if (str_course.startsWith("CM")) {
    return "CM";
  } else if (str_course.startsWith("TD")) {
    return "TD";
  } else if (str_course.startsWith("TP")) {
    return "TP";
  } else {
    alert("Erreur de fonction 'wichCourse'");
  }
}


Fonction qui va permettre de recharger les Events (quand on drop un CM, décrémenter de 1 à chaque fois)
Donc on va mettre à vide la partie external-events pour ensuite la recharger avec la fonction de base.



loadExtEvents();
/*
Fonction qui géré le drop si c'est un CM.
*/
/*function dropCM(el_ue, info_element) {

  el_ue.ajoutCM(info_element.date);
  reloadEvents();

}



Même principe que dropCM() sauf que ce n'est pas encore fait car il faut gérer le système de groupe,
pareil pour les TP

function dropTD(el_ue, info_element) {
  alert("ça marche td");
}

function dropTP(el_ue, info_element) {
  alert("ça marche tp");

}
*/

/*

TO-DO list :

* Faire en sorte que quand on drop un élément, on ait juste le code UE + le type (cm, td, tp)
  et non pas "CM(41)"

* Faire le dropTD() et dropTP() en fonction des groupes

* Mettre en place la possibilité de choisir un groupe pour les TD et TP
  Plusieurs possibilités :
    - pouvoir cliquer sur l'élément, une fois droppé, et selectionner un groupe
    - dans le menu des events draggables, lorsqu'on clique sur TD ou TP, avec une animation
      CSS, les events (TD-B, TD-A, ...) apparaissent (ce qu'on a commencé à voir)
    - pouvoir cliquer sur l'élément, avant de drag, et selectionner un groupe

Pour l'instant on n'a pas encore pensé à d'autres choses si mes souvenirs sont bons.

*/
//console.log("on est dans test.js , " , LiUE.get('HAI502I'));
