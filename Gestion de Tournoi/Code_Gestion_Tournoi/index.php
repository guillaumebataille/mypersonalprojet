<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Chill&Tea - Accueil</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Logo -->
  <link rel="icon" href="img/logo.png">
  <link rel="apple-touch-icon" href="img/logo.png">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">
</head>

<body>
  <div id="h">
    <div class="parallax"></div>
  

    <!-- /Menu de navigation -->

    <img class="logopng" src="img/logo.png" alt="Logo Chill&Tea">
    <div class="logo"><a href="">Chill&Tea</a></div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2 centered">
          <h1>Bienvenue sur Chill&Tea.<br/>Le site de tournoi réflechi pour <br/> les compétiteurs en tout genre.</h1>
          <div class="mtb">
            
      </form>
              <p class="text-center"><a href="/website/inscription/inscription.php" class="btn btn-conf btn-green">Envie de proposer ton equipe ? Inscription de capitaine ici</a></p>
          </div>
          <!--/mt-->
          <h6>Service gratuit pour les particuliers et les professionnels.</h6>
        </div>
      </div>
      <!--/row-->
    </div>
    <!--/container-->
    <div class="log"><a href="/website/connexion/connexion.php">Se connecter</a></div>
  </div>
  <!-- /H -->

  <div class="container ptb">
    <div class="row">
      <div class="col-md-6">
        <h2 class="h2">Créer. Gérer. Gagner !</h2>
        <p class="mt">Nos administrateurs utilisent les outils de créations de tournois et ont a disposition un large choix d'options pour créer des compétitions uniques. <br>
        Les gestionnaires de tournois quant à eux ont accès a de nombreuses options, y compris une page dédiée, pour pouvoir mettre à jour les compétitions.</p>
      </div>
      <div class="col-md-6">
        <div class="cup">
          <img src="img/cup.png" class="img-responsive mt" alt="Coupe">
        </div>
      </div>
    </div>
    <!--/row-->
  </div>
  <!--/container-->

  <div id="sep">
    <div class="container">
      <div class="row centered">
        <div class="col-md-8 col-md-offset-2">
          
          <h1>Consulter tout les tournois Chill&Tea !</h1>
          
          <h4>Découvre les résultats et les rencontres des tournois passés, en cours et à venir. Clique sur le bouton ci-dessous pour accéder à la page d'affichage des tournois.</h4>
          <p><a href="Tournoi/tournois.php" class="btn btn-conf-2 btn-green">Tournois</a></p>
        </div>
        <!--/col-md-8-->
      </div>
    </div>
  </div>
  <!--/sep-->

  <div id="g">
    <div class="container">
      <div class="row sponsor centered">
        <div class="col-sm-2 col-sm-offset-1">
          <img src="img/client1.png" alt="">
        </div>
        <div class="col-sm-2">
          <img src="img/client3.png" alt="">
        </div>
        <div class="col-sm-2">
          <img src="img/client2.png" alt="">
        </div>
        <div class="col-sm-2">
          <img src="img/client4.png" alt="">
        </div>
        <div class="col-sm-2">
          <img src="img/client5.jpg" alt="">
        </div>
      </div>
      <!--/row-->
    </div>
  </div>
  <!--/g-->

  <div id="f">
    <div class="container">
      <div class="row centered">
        <h2>Nous contacter !</h2>
        <h5>CHILLANDTEA@OUTLOOK.FR</h5>
        <a href="mailto:chillandtea@outlook.fr"><img class="mail" src="img/mail.png" alt="mail"></a>
      </div>
    </div>
    <!--/container-->
  </div>
  <!--/F-->

  <div id="copyrights">
    <div class="container">
      <p>
        &copy; Copyrights <strong>Chill&Tea</strong>. Tous droits réservés.
      </p>
      <div class="credits">
        <!-- Ne pas supprimer pour raison de droits d'auteurs ! -->

        <!-- =======================================================
        Template Name: SumoLanding
        Template URL: https://templatemag.com/sumolanding-bootstrap-landing-template/
        Author: TemplateMag.com
        License: https://templatemag.com/license/
        ======================================================= -->
        Créé avec la template SumoLanding par <a href="https://templatemag.com/">TemplateMag</a>
        <br/>
        Notre logo provient de <a href='https://pngtree.com/so/cute-vector'>PngTree</a> !

      </div>
    </div>
  </div>
  <!-- / copyrights -->

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/php-mail-form/validate.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>