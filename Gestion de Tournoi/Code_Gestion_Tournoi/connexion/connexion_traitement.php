<?php 
    session_start(); // Session utilis� pour rester connecter sur les diff�rents site / conserver la memoire
        require_once '../config.php';// Recuperation de config

    if(!empty($_POST['email']) && !empty($_POST['password'])) // Test pour voir si les valeurs email et mdp existe bien 
    {
        $email = htmlspecialchars($_POST['email']); // Recup l'email fourni par le submit de l'index
        $password = htmlspecialchars($_POST['password']);// Recup du password fourni par le submit de l'index

        $check = $bdd->prepare('SELECT Nom, Prenom, Genre, Email, Login, Password FROM utilisateur WHERE  email = ?'); 
        $check->execute(array($email)); 
        $data = $check->fetch();
        $row = $check->rowCount();

        if($row == 1)
        {
            if(filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                
                if(password_verify($password, $data['Password']))
                {
                    $_SESSION['utilisateur'] = $data['Email'];
                    header('Location: ../accueil.php');
                    die();
                }else{ header('Location: /website/connexion/connexion.php?login_err=password'); die(); } // Si l'erreur vient du password
            }else{ header('Location: /website/connexion/connexion.php?login_err=email'); die(); }// Si l'erreur vient de l'email
        }else{ header('Location: /website/connexion/connexion.php?login_err=already'); die(); }// Si le compte n'existe pas'
    }
