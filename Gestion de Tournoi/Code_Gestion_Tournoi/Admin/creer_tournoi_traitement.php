<?php 
    require_once '../config.php';
    
    session_start();
    if(!isset($_SESSION['utilisateur'])){
        header('Location:../index.php');
        die();
    }
  

    if(!empty($_POST['nom']) && !empty($_POST['discipline']) && !empty($_POST['date']) && !empty($_POST['lieu']) && !empty($_POST['nb_equipe']) && !empty($_POST['nb_jour']))
    {
        $nomT = htmlspecialchars($_POST['nom']);
        $disciplineT = htmlspecialchars($_POST['discipline']);
        $dateT = htmlspecialchars($_POST['date']);
        $lieuT = htmlspecialchars($_POST['lieu']);
        $nb_equipeT = htmlspecialchars($_POST['nb_equipe']);
        $nb_jourT = htmlspecialchars($_POST['nb_jour']);
        $type = htmlspecialchars($_POST['deftype']);

        // Recuperation de l'id via l'email/session
        $email = $_SESSION['utilisateur'];    
        $id = $bdd->prepare('SELECT * FROM utilisateur WHERE ( utilisateur.Email = ? )');
        $id->execute(array($email));
        $id_data = $id->fetch(); 
        $id_admin = $id_data['idIndividu'];  
       

        function is_Power($x, $y) // Fonction determinant si x est une puissance de y.
        {
          $a = $x;
          $b = $y;
            while ($x % $y == 0) {
                $x = $x / $y;
                }
       
	        return ($x == 1);
    
  
        }



      if ($nb_equipeT < 33){
        if(is_Power(+$nb_equipeT, 2) || ($type != "Coupe")){
                if(strlen($nomT) <= 50){
                    if(strlen($disciplineT) <= 50){
                        if(strlen($lieuT) <= 50){
          
                            $insert = $bdd->prepare('INSERT INTO tournoi VALUES (NULL, :Nom_Tournoi, :Type, :Discipline, :Date_Debut, :Lieu, :Nb_Equipe, :Nb_Jour, :idAdminTournoi)');
                            $insert->execute(array(
                                'Nom_Tournoi' => $nomT,
                                'Type' => $type,
                                'Discipline' => $disciplineT,
                                'Date_Debut' => $dateT,
                                'Lieu' => $lieuT,
                                'Nb_Equipe' => $nb_equipeT,
                                'Nb_Jour' => $nb_jourT,
                                'idAdminTournoi' => $id_admin ));

                            
                            // ici, nous avons créer le nouveau tournoi avec succès.     
                            // Maintenant, recuperons l'id du tournoi afin de determiner le nombre de tour necessaire.


                            $request_idTournoi = $bdd->prepare('SELECT * FROM tournoi WHERE ( Nom_Tournoi = ? )');
                            $request_idTournoi->execute(array($nomT));
                            $idTournoi_data = $request_idTournoi->fetch(); 
                            $idTournoi_recup = $idTournoi_data['idTournoi'];   
                            
                            
                                 $nbe = $nb_equipeT;
                                 $order = 1;

                                    if ($nbe == 2){
                                    
                                      $idTr = $bdd->prepare('SELECT * FROM tour WHERE ( tour.Nom_Tour = ? )'); 
                                      $idTr->execute(array("Finale"));
                                      $idTr_data = $idTr->fetch(); 
                                      $idT_Finale = $idTr_data['idTour']; 

                                      $insertTT = $bdd->prepare('INSERT INTO se_compose_de VALUES (:idTour, :idTournoi)');
                                      $insertTT->execute(array(
                                        'idTour' => $idT_Finale,
                                        'idTournoi' => $idTournoi_recup
                                        // La finale est ajouté si il n'y a que 2 équipes
                                            ));
                                        
                                        $insertmatch = $bdd->prepare('INSERT INTO tournoi.match VALUES (NULL, :idTournoi ,NULL ,NULL, NULL, :Ordre)');
                                        $insertmatch->execute(array(
                                        'idTournoi' => $idTournoi_recup,
                                        'Ordre' => $order )); // Création du tuple correspondant au match de la finale fonctionne

                                        $query_idMatch = $bdd->prepare('SELECT * FROM tournoi.match WHERE ( idTournoi = ? ) AND ( Ordre = ?) '); 
                                        $query_idMatch->execute(array($idTournoi_recup,$order)); 
                                        $idMatch_data = $query_idMatch->fetch();
                                        $idMatch_recup = $idMatch_data['idMatch'];// Recuperation de l'idMatch juste au dessus fonctionne

                                        $insertjoue = $bdd->prepare('INSERT INTO tournoi.joue VALUES (NULL ,NULL ,:idTour, :idMatch)');
                                        $insertjoue->execute(array(
                                        'idTour' => $idT_Finale,
                                        'idMatch' => $idMatch_recup )); // Création du tuple correspondant au match de la finale fonctionne

                                     
                                        }    
                                    
                                    else{ // Sinon, on itere 
                                        while ($nbe > 2) { // Tant que le nombre d'equipe est différent de 2 (donc qu'on est pas dans le cas finale + petite finale')'

                                            $request_idTour = $bdd->prepare('SELECT * FROM tour WHERE ( tour.Nb_Equipe = ? )');
                                            $request_idTour->execute(array($nbe));
                                            $idTour_data = $request_idTour->fetch(); 
                                            $idTour_recup = $idTour_data['idTour']; // idTour_recup = l'id du tour correspondant au nb d'equipe actuel

                                           
                                            $insertTT = $bdd->prepare('INSERT INTO se_compose_de VALUES (:idTour, :idTournoi, :Actuel)');
                                            $insertTT->execute(array(
                                                'idTour' => $idTour_recup,
                                                'idTournoi' => $idTournoi_recup
                                                'Actuel' => true;
                                                 )); // Insertion dans la table se_compose_de afin associer le tour et le tournoi
                                            
                                            // On a $idTour et $nbe, il va falloir iterer $nbe/2 fois pour avoir tout les matchs voulu
                                            // premiere iteration : 
                                            
                                           
                                            for ($nbiter = $nbe / 2; $nbiter > 0; $nbiter--) {
    
                                            $insertmatch = $bdd->prepare('INSERT INTO tournoi.match VALUES (NULL, :idTournoi ,NULL ,NULL, NULL, :Ordre)');
                                            $insertmatch->execute(array(
                                            'idTournoi' => $idTournoi_recup,
                                            'Ordre' => $order )); // Création du tuple correspondant au match de la finale fonctionne
                                                                  // Incrémentation de l'ordre pour le match suivant

                                            $query_idMatch = $bdd->prepare('SELECT * FROM tournoi.match WHERE ( idTournoi = ? ) AND ( Ordre = ?) '); 
                                            $query_idMatch->execute(array($idTournoi_recup,$order)); 
                                            $idMatch_data = $query_idMatch->fetch();
                                            $idMatch_recup = $idMatch_data['idMatch'];// Recuperation de l'idMatch juste au dessus fonctionne

                                            $insertjoue = $bdd->prepare('INSERT INTO tournoi.joue VALUES (NULL ,NULL ,:idTour, :idMatch)');
                                            $insertjoue->execute(array(
                                            'idTour' => $idTour_recup,
                                            'idMatch' => $idMatch_recup )); // Création du tuple correspondant au match de la finale fonctionne

                                            $order = $order + 1;
                                             }

                                            $nbe = ($nbe/2) ; // Et on passe au tour suivant (avec 2 fois moins d'équipes)
                                            }

                                            // Une fois arrivé a 2 équipes, on a une finale mais aussi une petite finale : 

                                        $idTu = $bdd->prepare('SELECT * FROM tour WHERE ( tour.Nom_Tour = ? )'); 
                                        $idTu->execute(array("Petite Finale"));
                                        $idTu_data = $idTu->fetch(); 
                                        $idT_PetiteFinale = $idTu_data['idTour']; 

                                        $insertTT = $bdd->prepare('INSERT INTO se_compose_de VALUES (:idTour, :idTournoi)');
                                        $insertTT->execute(array(
                                            'idTour' => $idT_PetiteFinale,
                                            'idTournoi' => $idTournoi_recup
                                             )); // ajout de la petite finale

                                        $insertmatch = $bdd->prepare('INSERT INTO tournoi.match VALUES (NULL, :idTournoi ,NULL ,NULL, NULL, :Ordre)');
                                        $insertmatch->execute(array(
                                        'idTournoi' => $idTournoi_recup,
                                        'Ordre' => $order )); // Création du tuple correspondant au match de la finale fonctionne

                                        $query_idMatch = $bdd->prepare('SELECT * FROM tournoi.match WHERE ( idTournoi = ? ) AND ( Ordre = ?) '); 
                                        $query_idMatch->execute(array($idTournoi_recup,$order)); 
                                        $idMatch_data = $query_idMatch->fetch();
                                        $idMatch_recup = $idMatch_data['idMatch'];// Recuperation de l'idMatch juste au dessus fonctionne

                                        $insertjoue = $bdd->prepare('INSERT INTO tournoi.joue VALUES (NULL ,NULL ,:idTour, :idMatch)');
                                        $insertjoue->execute(array(
                                        'idTour' => $idT_PetiteFinale,
                                        'idMatch' => $idMatch_recup )); // Création du tuple correspondant au match de la finale fonctionne
                                        $order = $order +1 ;

                                      
                                        $idTr = $bdd->prepare('SELECT * FROM tour WHERE ( tour.Nom_Tour = ? )'); 
                                        $idTr->execute(array("Finale"));
                                        $idTr_data = $idTr->fetch(); 
                                        $idT_Finale = $idTr_data['idTour']; 

                                        $insertTT = $bdd->prepare('INSERT INTO se_compose_de VALUES (:idTour, :idTournoi)');
                                        $insertTT->execute(array(
                                          'idTour' => $idT_Finale,
                                          'idTournoi' => $idTournoi_recup
                                              )); // ajout de la finale


                                        $insertmatch = $bdd->prepare('INSERT INTO tournoi.match VALUES (NULL, :idTournoi ,NULL ,NULL, NULL, :Ordre)');
                                        $insertmatch->execute(array(
                                        'idTournoi' => $idTournoi_recup,
                                        'Ordre' => $order )); // Création du tuple correspondant au match de la finale fonctionne

                                        $query_idMatch = $bdd->prepare('SELECT * FROM tournoi.match WHERE ( idTournoi = ? ) AND ( Ordre = ?) '); 
                                        $query_idMatch->execute(array($idTournoi_recup,$order)); 
                                        $idMatch_data = $query_idMatch->fetch();
                                        $idMatch_recup = $idMatch_data['idMatch'];// Recuperation de l'idMatch juste au dessus fonctionne

                                        $insertjoue = $bdd->prepare('INSERT INTO tournoi.joue VALUES (NULL ,NULL ,:idTour, :idMatch)');
                                        $insertjoue->execute(array(
                                        'idTour' => $idT_Finale,
                                        'idMatch' => $idMatch_recup )); // Création du tuple correspondant au match de la finale fonctionne
                                        

                                       }               
                            // Maintenant qu'on a l'idTournoi, on peut l'implémenter dans se compose de via un algo qui va implémenter chaque tour.

                            // addallTour_Tournoi($idT_tournoi,$nb_equipeT);
                          
                            
                            header('Location:creer_tournoi.php?reg_err=success');
                            die();
                        }else{ header('Location: creer_tournoi.php?reg_err=lieu_lenght'); die();}
                    }else{ header('Location: creer_tournoi.php?reg_err=discipline_length'); die();}
                }else{ header('Location: creer_tournoi.php?reg_err=nom_lenght'); die();}
        }else{ header('Location: creer_tournoi.php?reg_err=Pui2'); die();}
      }else{ header('Location: creer_tournoi.php?reg_err=tropequipe'); die();}  
    } 
