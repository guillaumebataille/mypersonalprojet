<?php

include_once '../config.php'; 
  session_start();
    if(!isset($_SESSION['utilisateur'])){
        header('Location:../index.php');
        die();
    }


     if ($_POST['nom_tournoi']){
        $_SESSION['nomT'] = htmlspecialchars($_POST['nom_tournoi']);} 

      

     
?>

<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="author" content="NoS1gnal"/>

            <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <title>Connexion</title>
        </head>
        <body>
        <div class="login-form">
            <?php 
                if(isset($_GET['reg_err']))
                {
                    $err = htmlspecialchars($_GET['reg_err']);

                    switch($err)
                    {
                        case 'success1':
                        ?>
                            <div class="alert alert-success">
                                <strong>Succès</strong> Affectation a bien eu lieu !
                            </div>
                        <?php
                        break;

                        case 'success2':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Attention</strong> Cet utilisateur est déja associé a ce tournoi !
                            </div>
                        <?php
                        break;

                        case 'success3':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> Cet utilisateur n'est pas inscrit, veuillez créer son compte gestionnaire !'
                            </div>
                        <?php
                       

                    }
                }
                ?>
            
            

                <?php

                $query = $bdd->prepare("SELECT * FROM tournoi"); 
                $query->execute();
                $query_data = $query->fetch(); 
                $query_data_bis = $query->fetchAll(PDO::FETCH_COLUMN, 1); 
                                                                                
                
            ?>



         <form action="Affectation_Gestio_traitement.php" method="post">
                <h2 class="text-center"> Vous avez selectionné le tournoi : <?php echo $_SESSION['nomT']?>  !  </h2>     
                <div class="form-group">
                
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Entrez l'email du gestionnaire ici" required="required" autocomplete="off">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Affectation </button>
                </div> 
                
                <input type="hidden" name="NomTournoi"  value= <?php echo $_SESSION['nomT'] ?>>
                
            </form>
             <p class="text-center"><a href="../accueil.php">Accueil</a></p>
            <p class="text-center"><a href="Affectation_Gestio.php">Retour selection tournoi</a></p>
            
            
           
        </div>
    

     
        <style>
            .login-form {
                width: 340px;
                margin: 50px auto;
            }
            .login-form form {
                margin-bottom: 15px;
                background: #f7f7f7;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                padding: 30px;
            }
            .login-form h2 {
                margin: 0 0 15px;
            }
            .form-control, .btn {
                min-height: 38px;
                border-radius: 2px;
            }
            .btn {        
                font-size: 15px;
                font-weight: bold;
            }
        </style>
        </body>
</html>