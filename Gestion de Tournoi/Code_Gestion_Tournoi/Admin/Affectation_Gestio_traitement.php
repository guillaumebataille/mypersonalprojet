<?php 
    require_once '../config.php';
    session_start();
    if(!isset($_SESSION['utilisateur'])){
        header('Location:../index.php');
        die();
    }
    
        $nomT = $_SESSION['nomT'];   // nomT recup par SESSION a bien été recup      ?>


                                               




    <?php


    if(!empty($_POST['email']) && !empty($_POST['NomTournoi']))
    {
        $email = htmlspecialchars($_POST['email']); // email a bien été recup
       


        
        $request_idTournoi = $bdd->prepare('SELECT idTournoi FROM tournoi WHERE Nom_Tournoi = ? '); // Recup l'idTournoi via nomT(le nom d'un tournoi')
        $request_idTournoi->execute(array($nomT));
        $idTournoi_data = $request_idTournoi->fetch(); 
        $idTournoi_recup = $idTournoi_data['idTournoi'];  // idTournoi_recup contient bien l'id du tournoi correspondant
        //var_dump($idTournoi_data);
        echo $idTournoi_recup;

        
        $request_idIndividu = $bdd->prepare('SELECT * FROM utilisateur WHERE email = ?'); // Recup l'id de quelqu'un via son email
        $request_idIndividu->execute(array($email));
        $data_idIndividu = $request_idIndividu->fetch();
        $row = $request_idIndividu->rowCount(); // row donne bien le nbr de ligne ou l'utilisateur a été declaré si il existe (1)'
        if ($row==1){$idGestio = $data_idIndividu['idIndividu'];} // idGestio est bien l'id correspondant a l'utilisateur si il existe
        else{$_SESSION['nomT'] = $nomT;
        header('Location: Affectation_Gestio_bisbis.php?reg_err=success3'); die();} // Si il n'existe pas (CAS 3 geré)
       
     
        
                                // On sait que l'utilisateur existe, on verifie maintenant si il est gestionnaire dans la table attributionrole
                                $query_role = $bdd->prepare("SELECT * FROM attributionrole WHERE ( idIndividu = ? ) AND (idRole = 2 ) ");
                                $query_role->execute(array($idGestio));
                                $role_data = $query_role->fetch();
                                $row_role = $query_role->rowCount(); //row_role donne exactement 1 si la personne est  gestionnaire et 0 si elle ne l'es pas
                               

                                if($row_role == 0){ // Si la personne n'est pas un gestionnaire, on la transforme en gestionnaire
                                        $request_role = $bdd->prepare('INSERT INTO attributionrole  VALUES(:idRole, :idIndividu)');
                                        $request_role->execute(array(
                                        'idRole' => 2,
                                        'idIndividu' => $idGestio
                                      ));} // Une personne existante mais non gestionnaire devient bien gestionnaire après cet INSERT
                                      
                                      // A ce stade, soit on vient de rendre individu gestionnaire, soit il l'était déja
                                      // Donc on vérifie si il est déja gestio sur ce tournoi, si oui, on sort le message d'erreur 4
                                      // Sinon, on l'affecte au tournoi : C'est un succès.
                                     
                                      

                                        // Note : Pour des SELECT a multiples variable php, c'est ? ? ? et les donner dans l'ordre par la suite.
                                        $request_organise = $bdd->prepare("SELECT * FROM organise WHERE ( idIndividuGestio = ? ) AND (idTournoi = ? ) "); 
                                        $request_organise->execute(array($idGestio,$idTournoi_recup));
                                        $organise_data = $request_organise->fetch();
                                        $row_organise = $request_organise->rowCount(); // row_organise donne bien 1 si individu est gestio de ce tournoi et 0 si il ne l'est pas.
                                       

                                        if ($row_organise == 0) { // Ici, comme l'individu n'est pas gestionnaire de ce tournoi, on peut l'ajouter

                                          $query_affectation = $bdd->prepare('INSERT INTO organise  VALUES(:idIndividuGestio, :idTournoi)'); // Et on l'affecte au tournoi selectionné precedement (CAS 1)
                                          $query_affectation->execute(array(
                                          'idIndividuGestio' => $idGestio ,
                                          'idTournoi' => $idTournoi_recup));  

                                          $_SESSION['nomT'] = $nomT;
                                          header('Location:Affectation_Gestio_bisbis.php?reg_err=success1');
                                          die();} 

                                        else { // Ici c'est si il est déja gestionnaire de ce tournoi, on retourne alors le (CAS 2) en msg d'erreur
                                          
                                            $_SESSION['nomT'] = $nomT;
                                          header('Location:Affectation_Gestio_bisbis.php?reg_err=success2');
                                               
                                          die();} 
                                        


    }
