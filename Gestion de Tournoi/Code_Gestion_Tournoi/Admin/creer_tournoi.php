
<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="author" content="NoS1gnal"/>

            <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <title>Creation_tournoi</title>
        </head>
        <body>
        <div class="login-form">
            <?php 
                if(isset($_GET['reg_err']))
                {
                    $err = htmlspecialchars($_GET['reg_err']);

                    switch($err)
                    {
                        case 'success':
                        ?>
                            <div class="alert alert-success">
                                <strong>Succès</strong> Création du tournoi reussie!
                            </div>
                        <?php
                        break;

                        case 'lieu_lenght':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> Lieu trop long !
                            </div>
                        <?php
                        break;

                        case 'discipline_lenght':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> Discipline trop longue !
                            </div>
                        <?php
                        break;

                        case 'nom_length':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> Nom trop long !
                            </div>
                        <?php 
                        break;

                          case 'tropequipe':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> Trop equipe (max 32) !
                            </div>
                        <?php 
                        break;

                        case 'Pui2':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> Coupe = pui de 2 équipes ! 
                            </div>
                        <?php 
					
                        ?>
                          
                        <?php 

                    }
                }
                ?>
            
            <form action="creer_tournoi_traitement.php" method="post">

                <h2 class="text-center">Creation d'un tournoi </h2>       

                <div class="form-group">


                <!-- Nom tournoi text  -->
				 <div class="form-group">
                    <input type="text" name="nom" class="form-control" placeholder="Nom" required="required" autocomplete="off">
                </div>
				
				
                 <!-- Type coupe radio 1  -->
				<div>
					<input type="radio" id="coupe" name="deftype" value="Coupe" checked>
					<label for="coupe">Coupe</label>
				</div>
				

				<!--  Type championnat 2 -->
				<div>
					<input type="radio" id="championnat" name="deftype" value="Championnat">
					<label for="championnat">Championnat</label>
				</div>
				

				<!-- Type mixte 3   -->
				<div>
					<input type="radio" id="mixte" name="deftype" value="Mixte">
					<label for="mixte">Mixte</label>
				</div>


                <!-- Discipline text -->
                <div class="form-group">
                    <input type="text" name="discipline" class="form-control" placeholder="Discipline" required="required" autocomplete="off">
                </div>


                <!-- Date debut date // avec la date egale ou sup a la date d'aujourdhui -->
                <div class="form-group">
                    <p class="text">Date de debut : </p>
                    <input type="date" name="date" class="form-control" placeholder="yyyy-mm-dd" min=<?php echo date('Y-m-d'); ?> required="required" autocomplete="off"> 

                </div>


                <!-- Lieu text  -->
                <div class="form-group">
                    <input type="text" name="lieu" class="form-control" placeholder="Lieu du tournoi" required="required" autocomplete="off">
                </div>
                

                <!-- Nb equipe // min a 2 -->
                <div class="form-group">
                    <input type="number" name="nb_equipe" min = 2 class="form-control" placeholder="Entrez le nombre d'équipe" required="required" autocomplete="off">
                </div>


                <!-- Nb jour // min a 1 -->
                <div class="form-group">
                    <input type="number" name="nb_jour" min = 1 class="form-control" placeholder="Durée en jour" required="required" autocomplete="off">
                </div>


                <!-- Submit les données à : creer_tournoi_traitement.php -->
                 <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"> Créer le tournoi </button>
                </div> 
         
            </form>

           
            <p class="text-center"><a href="../accueil.php">Accueil</a></p>
        </div>
 
            
        <style>
            .login-form {
                width: 340px;
                margin: 50px auto;
            }
            .login-form form {
                margin-bottom: 15px;
                background: #f7f7f7;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                padding: 30px;
            }
            .login-form h2 {
                margin: 0 0 15px;
            }
            .form-control, .btn {
                min-height: 38px;
                border-radius: 2px;
            }
            .btn {        
                font-size: 15px;
                font-weight: bold;
            }
        </style>
        </body>
</html>