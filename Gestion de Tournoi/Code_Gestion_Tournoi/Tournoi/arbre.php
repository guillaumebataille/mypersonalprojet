<!html>


<script type="text/javascript" src="jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="jquery.bracket.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="https://www.aropupu.fi/bracket/jquery-bracket/dist/jquery.bracket.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://www.aropupu.fi/bracket/jquery-bracket/dist/jquery.bracket.min.css" />


 <span id="matchCallback"></span>
<div id="matches">
  <div class="demo">
  </div>
</div>


<!-- <div id="matchesblank">
  <div class="demo">
  </div>
</div> -->




<script>

// Il va falloir recup le id du tournoi actuel


//Avec cet id on va recuperer l'idTour le plus grand du tournoi dans se-compose-de pour savoir bc de fois on va iterer


// On verifie si le tournoi est deja initialis� 
// Si oui on cr�er le tournoi vide
// Si non, retour a la page precedent avec message d'erreur.

var matchData = {           
  teams : [
    ["Equipe 1", "Equipe 2"], // ligne joue ordre 1 
    ["Equipe 3", "Equipe 4"], // ligne joue ordre 2
    ["Equipe 5", "Equipe 6"], // ligne joue ordre 3
    ["Equipe 7", "Equipe 8"], // ligne joue ordre 4
    
    
  ],

  
  results : [
    //premier tour  iteration nbequipe du tour / 2 et chaque iteration on donne le score 1 et score 2 + 'match + 'ordre (avec ordre =1) et chaque ligne ont faire ordre +1 avant de re recup le score du match suivant
    [
      [4,3,'Match 1'], //Recup des score 1 et 2 ordre 1
      [1,4,'Match 2'], //Recup des score 1 et 2 ordre 2
      [1,4,'Match 3'], //Recup des score 1 et 2 ordre 3
      [1,4,'Match 4']  //Recup des score 1 et 2 ordre 4
      
    ],
    //le tour etant fini on affiche la suite
    //demi finale 
     [
      [], 
      [], 

    ],
  
    //fourth round - Final
     [
      [], //winners
      [] //third place
    ]
  ]
}

var matchBlankData = {
  teams : [
    ["Open Slot", "Open Slot"],
    ["Open Slot", "Team 4"],
    ["Team 5", "Team 6"],
    ["Team 7", "Team 8"],
    ["Team 9", "Team 10"],
    ["Team 11", "Team 12"],
    ["Team 13", "Team 14"],
    ["Team 15", "Team 16"]
  ],
  results : [


  
 
  ]
}
 /* var matchData = {           
  teams : [
    ["Equipe 1", "Equipe 2"], 
    ["Equipe 3", "Equipe 4"],
    ["Equipe 5", "Equipe 6"],
    ["Equipe 7", "Equipe 8"],
    
  ],

  // Il va falloir afficher un tournoi vide en entier pour le dessiner en entier avec les score vide (le tournoi doit etre initialis�)
  results : [
    //premier tour  iteration nbequipe du tour / 2 et chaque iteration on donne le score 1 et score 2 + 'match + 'ordre (avec ordre =1) et chaque ligne ont faire ordre +1 
    [
      [4,3,'Match 1'], 
      [1,4,'Match 2'], 
      [1,4,'Match 3'], 
      [1,4,'Match 4'] 
      
    ],
    //le tour etant fini on affiche la suite
    //demi finale 
     [
      [4,3,'Match 9'], 
      [1,4,'Match 10'], 

    ],
    //third round - Semi Final
     [
      [4,3,'Match 13'], 
      [1,4,'Match 14']
    ],
    //fourth round - Final
     [
      [], //winners
      [1,4,'Match 16'] //third place
    ]
  ]
}*/
function onclick(data) {
  $('#matchCallback').text("onclick(data: '" + data + "')")
}
 
function onhover(data, hover) {
  $('#matchCallback').text("onhover(data: '" + data + "', hover: " + hover + ")")
}
 
$(function() {
  $('#matches .demo').bracket({  // Initialise le tournoi avec :
    init: matchData,             // La liste des equipes dans un var
   // onMatchClick: onclick,       // evenement pour montrer les donn�es associ� au deux equipes du match
   // onMatchHover: onhover        // evenement pour montrer les donn�es associ�
  })
  
  $('#matchesblank .demo').bracket({
    init: matchBlankData,
    onMatchClick: onclick,
    onMatchHover: onhover
  })
})
</script>
</html>