<?php
require_once '../config.php';
session_start();



$email = $_SESSION['utilisateur'];
$role = $bdd->prepare('SELECT * FROM utilisateur, role, attributionrole WHERE ( utilisateur.idIndividu = attributionrole.idIndividu ) AND ( attributionrole.idRole = role.idRoleR ) AND ( utilisateur.Email = ? ) ORDER BY idRoleR');
$roleisOk = $role->execute(array($email));
$rolerecup = $role->fetch();
$roleupdate = $rolerecup['Nom_Role'];


$list_tournament = $bdd->prepare('SELECT * FROM tournoi ORDER BY date_debut DESC ');
$list_tournament->execute();
// fetch all rows into an array.
$rows = $list_tournament->fetchAll();


if ($roleupdate === 'Capitaine'){
    $result = $bdd->prepare('SELECT * FROM Equipe WHERE (idIndividuCapitaine = ?)');
    $result->execute(array($rolerecup['idIndividu']));
    $team = $result->fetch();
}


?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Chill&Tea - Accueil</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Logo -->
    <link rel="icon" href="../img/logo.png">
    <link rel="apple-touch-icon" href="../img/logo.png">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">


    <!-- Libraries CSS Files -->
    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="../css/style.css" rel="stylesheet">
</head>

<body>
<div id="h">
    <div class="parallax"></div>
    <!-- Menu de navigation -->
    <div class="logo"><a href="../accueil.php">Chill&Tea</a></div>
    <div><a href="../accueil.php" class="btn btn-default btn-lg">Retour</a></div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-12 centered">


                <style>
                    table {
                        font-family: arial, sans-serif;
                        border-collapse: collapse;
                        width: 100%;
                    }

                    td, th {
                        border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;
                    }

                    tr:nth-child(even) {
                        background-color: #dddddd;
                    }
                </style>

                <table>
                    <tr>
                        <th>Nom</th>
                        <th>Type</th>
                        <th>Discipline</th>
                        <th>Nombre équipe</th>
                        <th>Lieu</th>
                        <th>Date du début</th>
                        <th>Admin</th>
                        <th>Manager</th>
                        <th>Option</th>
                    </tr>
                    <?php
                    foreach ($rows as $rs) {

                        $result = $bdd->prepare('SELECT * FROM utilisateur where 	idIndividu = ?');
                        $result->execute(array($rs['idAdminTournoi']));
                        $data = $result->fetch();
                        $name_admin = $data['Nom'] . " " . $data['Prenom'];

                        $result = $bdd->prepare('SELECT * FROM organise where idTournoi = ?');
                        $result->execute(array($rs['idTournoi']));
                        $data = $result->fetch();
                        $count = $result->rowCount();

                        if($count>0){
                            $result = $bdd->prepare('SELECT * FROM utilisateur where 	idIndividu = ?');
                            $result->execute(array($data['idIndividuGestio']));
                            $data = $result->fetch();
                            $name_manager = $data['Nom'] . " " . $data['Prenom'];
                        }else{
                            $name_manager ="";
                        }


                        $result = $bdd->prepare('SELECT * FROM est_inscrite where idTournoi = :idTournament AND idEquipe = :idTeam ');
                        $result->execute(array(
                            'idTeam' => $team['idEquipe'],
                            'idTournament' => $rs['idTournoi']
                        ));
                        $data = $result->fetch();
                        $count = $result->rowCount();
                        $subscribed = 0;
                        if($count>0){
                            $subscribed = 1;
                        }



                        echo '<tr>';
                        echo '<td><a href=detail_tournoi.php?id=' . $rs['idTournoi'] . '>' . $rs['Nom_Tournoi'] . '</a></td>';
                        echo '<td>' . $rs['Type'] . '</td>';
                        echo '<td>' . $rs['Discipline'] . '</td>';
                        echo '<td>' . $rs['Nb_Equipe'] . '</td>';
                        echo '<td>' . $rs['Lieu'] . '</td>';
                        echo '<td>' . $rs['Date_Debut'] . '</td>';
                        echo '<td>' .$name_admin. '</td>';
                        echo '<td>' .$name_manager. '</td>';
                        echo '<td>' ;
                        if($subscribed == 0){
                            echo "<form method='POST'><input type=hidden name=id value=".$rs['idTournoi']." ><input type=submit value=S'incrire name=subcribe_team ></form>";
                        }else{
                            echo 'Inscrit';
                        }
                        echo '</td>';
                        echo '</tr>';
                    }
                    ?>
                </table>

            </div>
        </div>
    </div>
</div>

<?php


if(isset($_POST['subcribe_team'])) {
    $id = $_POST['id'];
    $insert = $bdd->prepare('INSERT INTO est_inscrite (idEquipe, idTournoi,Valide) VALUES(:idTeam, :idTournament, :Valider )');
    $insert->execute(array(
        'idTeam' => $team['idEquipe'],
        'idTournament' => $id,
        'Valider' => 0
    ));
    echo "<meta http-equiv='refresh' content='0'>";
}

?>
<!--/container-->

<div id="g">
    <div class="container">
        <div class="row sponsor centered">
            <div class="col-sm-2 col-sm-offset-1">
                <img src="../img/client1.png" alt="">
            </div>
            <div class="col-sm-2">
                <img src="../img/client3.png" alt="">
            </div>
            <div class="col-sm-2">
                <img src="../img/client2.png" alt="">
            </div>
            <div class="col-sm-2">
                <img src="../img/client4.png" alt="">
            </div>
            <div class="col-sm-2">
                <img src="../img/client5.jpg" alt="">
            </div>
        </div>
        <!--/row-->
    </div>
</div>
<!--/g-->

<div id="f">
    <div class="container">
        <div class="row centered">
            <h2>Nous contacter !</h2>
            <h5>CHILLANDTEA@OUTLOOK.FR</h5>
            <a href="mailto:chillandtea@outlook.fr"><img class="mail" src="../img/mail.png" alt="mail"></a>
        </div>
    </div>
    <!--/container-->
</div>
<!--/F-->

<div id="copyrights">
    <div class="container">
        <p>
            &copy; Copyrights <strong>Chill&Tea</strong>. Tous droits réservés.
        </p>
        <div class="credits">
            <!-- Ne pas supprimer pour raison de droits d'auteurs ! -->

            <!-- =======================================================
            Template Name: SumoLanding
            Template URL: https://templatemag.com/sumolanding-bootstrap-landing-template/
            Author: TemplateMag.com
            License: https://templatemag.com/license/
            ======================================================= -->
            Créé avec la template SumoLanding par <a href="https://templatemag.com/">TemplateMag</a>
            <br/>
            Notre logo provient de <a href='https://pngtree.com/so/cute-vector'>PngTree</a> !

        </div>
    </div>
</div>
<!-- / copyrights -->

<!-- JavaScript Libraries -->
<script src="../lib/jquery/jquery.min.js"></script>
<script src="../lib/bootstrap/js/bootstrap.min.js"></script>
<script src="../lib/php-mail-form/validate.js"></script>

<!-- Template Main Javascript File -->
<script src="../js/main.js"></script>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>


</body>
</html>