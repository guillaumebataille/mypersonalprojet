<?php
require_once '../config.php';
session_start();
if (!isset($_SESSION['utilisateur'])) {
    header('Location:index.php');
    die();
}

$email = $_SESSION['utilisateur'];
$role = $bdd->prepare('SELECT * FROM utilisateur, role, attributionrole WHERE ( utilisateur.idIndividu = attributionrole.idIndividu ) AND ( attributionrole.idRole = role.idRoleR ) AND ( utilisateur.Email = ? ) ORDER BY idRoleR');
$roleisOk = $role->execute(array($email));
$rolerecup = $role->fetch();


$team = $bdd->prepare('SELECT * FROM Equipe WHERE (idIndividuCapitaine = ?)');
$team->execute(array($rolerecup['idIndividu']));
$data = $team->fetch();
$teamName = $data['Nom_Equipe']

?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Chill&Tea - Accueil</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Logo -->
    <link rel="icon" href="../img/logo.png">
    <link rel="apple-touch-icon" href="../img/logo.png">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">


    <!-- Libraries CSS Files -->
    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="../css/style.css" rel="stylesheet">
</head>

<body>
<div id="h">
    <div class="parallax"></div>
    <!-- Menu de navigation -->
    <div class="logo"><a href="../accueil.php">Chill&Tea</a></div>
    <div><a href="../accueil.php" class="btn btn-default btn-lg">Retour</a></div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-12 centered">
                <?php

                if(isset($_POST['delete_player'])) {
                    $id = $_POST['id'];
                    $team = $bdd->prepare('DELETE FROM est_membre_de WHERE (idJoueur = ?)');
                    $team->execute(array($id));
                    $team = $bdd->prepare('DELETE FROM Utilisateur WHERE (idIndividu = ?)');
                    $team->execute(array($id));
                    echo "<meta http-equiv='refresh' content='0'>";
                }
                ?>


                <div class="panel panel-default">
                    <div class="panel-heading">Mon equipe : <b><?php echo $teamName; ?></b></div>
                    <div class="panel-body">

                        <?php
                        $list_team = $bdd->prepare('SELECT Utilisateur.idIndividu, Utilisateur.Genre, Utilisateur.Nom, Utilisateur.Prenom FROM Equipe, Utilisateur, est_membre_de WHERE (Equipe.idEquipe = est_membre_de.idEquipe) AND (est_membre_de.idJoueur = utilisateur.idIndividu) AND (Equipe.idIndividuCapitaine = ?)');
                        $list_team->execute(array($rolerecup['idIndividu']));
                        // fetch all rows into an array.
                        $rows = $list_team->fetchAll();
                        ?>

                        <style>
                            table {
                                font-family: arial, sans-serif;
                                border-collapse: collapse;
                                width: 100%;
                            }

                            td, th {
                                border: 1px solid #dddddd;
                                text-align: left;
                                padding: 8px;
                            }

                            tr:nth-child(even) {
                                background-color: #dddddd;
                            }
                        </style>

                        <table>
                            <tr>
                                <th>Genre</th>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>option</th>
                            </tr>
                            <?php
                            foreach ($rows as $rs) {
                                echo '<tr>';
                                echo '<td>' . $rs['Genre'] . '</td>';
                                echo '<td>' . $rs['Nom'] . '</td>';
                                echo '<td>' . $rs['Prenom'] . '</td>';
                                echo '<td>';
                                echo "<form method='POST'><input type=hidden name=id value=".$rs["idIndividu"]." ><input type=submit value=Delete name=delete_player ></form>";
                                echo '</tr>';
                            }

                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--/container-->

<div id="g">
    <div class="container">
        <div class="row sponsor centered">
            <div class="col-sm-2 col-sm-offset-1">
                <img src="../img/client1.png" alt="">
            </div>
            <div class="col-sm-2">
                <img src="../img/client3.png" alt="">
            </div>
            <div class="col-sm-2">
                <img src="../img/client2.png" alt="">
            </div>
            <div class="col-sm-2">
                <img src="../img/client4.png" alt="">
            </div>
            <div class="col-sm-2">
                <img src="../img/client5.jpg" alt="">
            </div>
        </div>
        <!--/row-->
    </div>
</div>
<!--/g-->

<div id="f">
    <div class="container">
        <div class="row centered">
            <h2>Nous contacter !</h2>
            <h5>CHILLANDTEA@OUTLOOK.FR</h5>
            <a href="mailto:chillandtea@outlook.fr"><img class="mail" src="../img/mail.png" alt="mail"></a>
        </div>
    </div>
    <!--/container-->
</div>
<!--/F-->

<div id="copyrights">
    <div class="container">
        <p>
            &copy; Copyrights <strong>Chill&Tea</strong>. Tous droits réservés.
        </p>
        <div class="credits">
            <!-- Ne pas supprimer pour raison de droits d'auteurs ! -->

            <!-- =======================================================
            Template Name: SumoLanding
            Template URL: https://templatemag.com/sumolanding-bootstrap-landing-template/
            Author: TemplateMag.com
            License: https://templatemag.com/license/
            ======================================================= -->
            Créé avec la template SumoLanding par <a href="https://templatemag.com/">TemplateMag</a>
            <br/>
            Notre logo provient de <a href='https://pngtree.com/so/cute-vector'>PngTree</a> !

        </div>
    </div>
</div>
<!-- / copyrights -->

<!-- JavaScript Libraries -->
<script src="../lib/jquery/jquery.min.js"></script>
<script src="../lib/bootstrap/js/bootstrap.min.js"></script>
<script src="../lib/php-mail-form/validate.js"></script>

<!-- Template Main Javascript File -->
<script src="../js/main.js"></script>




<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>


</body>
</html>