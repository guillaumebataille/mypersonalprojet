<?php

include_once '../config.php'; 
  session_start();
    if(!isset($_SESSION['utilisateur'])){
        header('Location:../index.php');
        die();
    }

    $nomT =$_SESSION['nomT'];
    //echo $nomT;
?>

<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="author" content="NoS1gnal"/>

            <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <title>Connexion</title>
        </head>
        <body>
        <div class="login-form">
            <?php 
                if(isset($_GET['reg_err']))
                {
                    $err = htmlspecialchars($_GET['reg_err']);

                    switch($err)
                    {
                        case 'success':
                        ?>
                            <div class="alert alert-success">
                                <strong>Succès</strong> Les equipes ont bien été enregistré dans leurs matchs du 1er tour !
                            </div>
                        <?php
                        break;
                   
                        case 'sameequipe':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> Les equipes 1 & 2 sont similaires ! Veuillez choisir deux equipes différentes !
                            </div>
                        <?php
                       

                    }
                }
                ?>
                  
                <?php
             
                $request_idTournoi = $bdd->prepare('SELECT * FROM tournoi WHERE Nom_Tournoi = ? '); // Recup l'idTournoi via nomT(le nom d'un tournoi')
                $request_idTournoi->execute(array($nomT));
                $idTournoi_data = $request_idTournoi->fetch(); 
                $idTournoi_recup = $idTournoi_data['idTournoi'];  // idTournoi_recup contient bien l'id du tournoi correspondant
                $idTournoi_equipe = $idTournoi_data['Nb_Equipe'];


                //Essayons de trouver le nbr d 'equipes etant déja saisie dans joue
                $request_equipevalide = $bdd->prepare('SELECT * FROM joue, tournoi , tournoi.match WHERE (Nom_Tournoi = ? ) AND (tournoi.idTournoi = match.idTournoi) AND (match.idMatch = joue.idMatch) AND idEquipe1 IS NOT NULL AND idEquipe2 IS NOT NULL '); // Recup l'idTournoi via nomT(le nom d'un tournoi')
                $request_equipevalide->execute(array($nomT));
                $Equipevalide = $request_equipevalide->fetch(); 
                $row = 2 * $request_equipevalide->rowCount();  // row donne bien le nb d'equipe etant deja saisie dans joue

                 //REcuperation de l'idTourmax du tournoi 
                $query_Tour = $bdd->prepare('SELECT MAX(se_compose_de.idTour) FROM se_compose_de WHERE (idTournoi = ? ) ');
                $query_Tour->execute(array($idTournoi_recup));
                $Tour_data = $query_Tour->fetch();
                $idTourmax = $Tour_data['MAX(se_compose_de.idTour)']; // idTourmax donne bien l'idTour max pour un idTournoi donné 
                //echo $idTourmax;

                $_SESSION['idTourmax'] = $idTourmax;

                //REcuperation de du nom de l'idTourmax : c'est le premier tour !
                $query_nomTour = $bdd->prepare('SELECT Nom_Tour FROM tour WHERE idTour = ?');
                $query_nomTour->execute(array($_SESSION['idTourmax']));
                $nomTour_data = $query_nomTour->fetch();
                $nomTourmax = $nomTour_data['Nom_Tour']; // nomTourmax donne bien le nom correspondant a idTourmax !
                


                $email = $_SESSION['utilisateur'];    
                $id = $bdd->prepare('SELECT * FROM utilisateur WHERE ( Email = ? )');
                $id->execute(array($email));
                $id_data = $id->fetch(); 
                $id_gestio = $id_data['idIndividu'];  
                 


                $query = $bdd->prepare('SELECT Nom_Equipe FROM tournoi, est_inscrite, equipe WHERE (Nom_Tournoi = ? ) AND (tournoi.idTournoi = est_inscrite.idTournoi) AND (est_inscrite.idEquipe = equipe.idEquipe) AND (est_inscrite.Valide = true) AND equipe.idEquipe != ALL (SELECT joue.idEquipe1 FROM joue, tournoi.match, tournoi WHERE (Nom_Tournoi = ?) AND (tournoi.idTournoi = match.idTournoi) AND (match.idMatch) = (joue.idMatch) AND idEquipe1 IS NOT NULL) AND equipe.idEquipe != ALL (SELECT idEquipe2 FROM joue, tournoi.match, tournoi WHERE (Nom_Tournoi = ?) AND (tournoi.idTournoi =match.idTournoi) AND (match.idMatch = joue.idMatch) AND idEquipe2 IS NOT NULL)');// Prepare la requete
                $query->execute(array($nomT,$nomT,$nomT));  
                $query_data_bis = $query->fetchAll(PDO::FETCH_COLUMN, 0); 
                   
                
              

                
                $query_match= $bdd->prepare('SELECT Ordre FROM joue, tournoi , tournoi.match WHERE (tournoi.Nom_Tournoi = ? ) AND (joue.idTour = ?) AND (tournoi.idTournoi = match.idTournoi) AND (match.idMatch = joue.idMatch) AND idEquipe1 IS NULL AND idEquipe2 IS NULL ');// Prepare la requete
                $query_match->execute(array($nomT,$_SESSION['idTourmax'])); // Execute la requete
                
                $query_matchbis = $query_match->fetchAll(PDO::FETCH_COLUMN, 0);
                

            ?>

                                           
       

       <form method="post" action="saisie_rencontreT1bis_traitement.php">
        <p class="text-center"> Vous avez choisi le tournoi </br> <?php echo $nomT  ?> </p> 
        <p class="text-center"> Vous avez </br> <?php echo $row ?> / <?php echo $idTournoi_equipe  ?> equipes saisie pour le tour : <?php   ?>  </p> 
        
       <p> Equipe 1 </p> 
       <select class="text-center" name="equipe1">
     <?php
      for($i=0;$i<count($query_data_bis);$i++) // compte le nombre de case d'array'
      {
           
           if($query_data_bis[$i] == $_POST['Nom_Equipe'])
                { $selected = ' selected'; }
           else { $selected = ''; }

           ?> 
           
           <option value="<?php echo $query_data_bis[$i];?>" <?php echo $selected;?>>
      <?php echo $query_data_bis[$i]; ?></option>
           <?php
      }
      
 ?>
   </select>

   </br>
   </br>

   <p> Equipe 2 </p> 
       <select class="text-center" name="equipe2">
     <?php
      for($i=0;$i<count($query_data_bis);$i++) // compte le nombre de case d'array'
      {
           
           if($query_data_bis[$i] == $_POST['Nom_Equipe'])
                { $selected = ' selected'; }
           else { $selected = ''; }

           ?> 
           
           <option value="<?php echo $query_data_bis[$i];?>" <?php echo $selected;?>>
      <?php echo $query_data_bis[$i]; ?></option>
           <?php
      }
     
 ?>
   </select>

   <p> Premier tour !</br> Les matchs de <?php echo $nomTourmax ?> disponibles ici : </p> 
       <select class="text-center" name="num_match">
     <?php
      for($i=0;$i<count($query_matchbis);$i++) // compte le nombre de case d'array'
      {
           
           if($query_matchbis[$i] == $_POST['Nom_Equipe'])
                { $selected = ' selected'; }
           else { $selected = ''; }

           ?> 
           
           <option value="<?php echo $query_matchbis[$i];?>" <?php echo $selected;?>>
           match n° <?php echo $query_matchbis[$i]; ?></option>
           <?php
      }
      
 ?>
   </select>

                </br>
                </br>

                <div class="form-group">
                    <?php $now = new DateTime() ?>
                    <input type="datetime-local" name="date_horaire" class="form-control" min='2021-05-12T12:00' required="required" autocomplete="off">
                    
                   
                </div>

   <input type="submit" value="check">
   </form>

  


    

 
            <p class="text-center"><a href="../accueil.php">Accueil</a></p>
            <p class="text-center"><a href="saisie_rencontreT1.php">Retour a la selection de mes tournois</a></p>
        </div>
 
            
        <style>
            .login-form {
                width: 340px;
                margin: 50px auto;
            }
            .login-form form {
                margin-bottom: 15px;
                background: #f7f7f7;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                padding: 30px;
            }
            .login-form h2 {
                margin: 0 0 15px;
            }
            .form-control, .btn {
                min-height: 38px;
                border-radius: 2px;
            }
            .btn {        
                font-size: 15px;
                font-weight: bold;
            }
        </style>
        </body>
</html>