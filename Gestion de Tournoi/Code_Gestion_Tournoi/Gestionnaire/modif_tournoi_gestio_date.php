<?php

include_once '../config.php'; 
  session_start();
    if(!isset($_SESSION['utilisateur'])){
        header('Location:../index.php');
        die();
    }



    if(!empty($_POST['nom_tournoi']))
    {
       
        $nomT = htmlspecialchars($_POST['nom_tournoi']); // nomT a bien été recup
        $newdateT = htmlspecialchars($_POST['newdate-tournoi']); // nomT a bien été recup


        $request_idTournoi = $bdd->prepare('SELECT * FROM tournoi WHERE Nom_Tournoi = ? '); // Recup l'idTournoi via nomT(le nom d'un tournoi')
        $request_idTournoi->execute(array($nomT));
        $idTournoi_data = $request_idTournoi->fetch(); 
        $idTournoi_recup = $idTournoi_data['idTournoi'];  // idTournoi_recup contient bien l'id du tournoi correspondant
        //echo  $idTournoi_recup;
     
       
        
        $query_update_nom = $bdd->prepare('UPDATE tournoi SET Date_Debut = ? WHERE (tournoi.idTournoi = ?) '); // Et on l'affecte au tournoi selectionné precedement (CAS 1)
        $query_update_nom->execute(array($newdateT,$idTournoi_recup));
        
        $_SESSION['nomT'] = $nomT;
        header('Location:modif_tournoi_gestio_bisbis.php?reg_err=successdate');
                                               
                                          die();}