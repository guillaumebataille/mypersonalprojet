<?php

include_once '../config.php'; 
  session_start();
    if(!isset($_SESSION['utilisateur'])){
        header('Location:../index.php');
        die();
    }


if ($_POST['nom_tournoi']){
        $_SESSION['nomT'] = htmlspecialchars($_POST['nom_tournoi']);}
?>

<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="author" content="NoS1gnal"/>

            <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <title>Connexion</title>
        </head>
        <body>
        <div class="login-form">
    
                <?php

                $email = $_SESSION['utilisateur']; 
                $nomT = $_POST['nom_tournoi'];     
                $id = $bdd->prepare('SELECT * FROM utilisateur WHERE ( Email = ? )');
                $id->execute(array($email));
                $id_data = $id->fetch(); 
                $id_gestio = $id_data['idIndividu'];  
                


                $query = $bdd->prepare("SELECT * FROM tournoi, organise WHERE (organise.idTournoi = tournoi.idTournoi ) AND (idIndividuGestio = ?) "); 
                $query->execute(array($id_gestio)); 
                
                $query_data_bis = $query->fetchAll(PDO::FETCH_COLUMN, 1); 

                 $query_tournoi = $bdd->prepare("SELECT * FROM tournoi WHERE (Nom_Tournoi = ?)");
                 $query_tournoi->execute(array($nomT));
                 $data_tournoi = $query_tournoi->fetch();
                 $data_tournoi_date = $data_tournoi['Date_Debut'];
                 $data_tournoi_nbjour = $data_tournoi['Nb_Jour'];

               

            ?>

                                           
        <h2 class="text-center"> Vous avez choisi le tournoi : <?php echo $nomT ?>   ! </h2> 

       <form method="post" action="modif_tournoi_gestio_name.php">
       <p class="text-center"> Le nom actuel du tournoi est : <?php echo $nomT ?> </p>
       <input type="text" name="newnom-tournoi" class="form-control" placeholder="Modifier nom ici" required="required" autocomplete="off">
       <input type ="hidden" name="nom_tournoi" value="<?php echo $nomT ?>">
       <input type="submit" value="submit">
       </form>


       <form method="post" action="modif_tournoi_gestio_date.php">
       <p class="text-center"> La date du tournoi  : <?php echo $data_tournoi_date ?> </p>
       <input type="date" name="newdate-tournoi" class="form-control" placeholder="yyyy-mm-dd" min="Y-m-d"  required="required" autocomplete="off">
       <input type ="hidden" name="nom_tournoi" value="<?php echo $nomT ?>">
       <input type="submit" value="submit">
       </form>


       <form method="post" action="modif_tournoi_gestio_duree.php">
       <p class="text-center"> La durée en jour du tournoi : <?php echo $data_tournoi_nbjour ?> </p>
       <input type="number" name="newnbjour-tournoi" class="form-control" placeholder="Modifier duree ici " min="1" required="required" autocomplete="off">
       <input type ="hidden" name="nom_tournoi" value="<?php echo $nomT ?>">
       <input type="submit" value="submit">
       </form>
         

    
            <p class="text-center"><a href="../accueil.php"> Retour à l'Accueil</a></p>
            <p class="text-center"><a href="modif_tournoi_gestio.php"> Retour à la selection des tournois à gérer</a></p>
        </div>
 
            
        <style>
            .login-form {
                width: 340px;
                margin: 50px auto;
            }
            .login-form form {
                margin-bottom: 15px;
                background: #f7f7f7;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                padding: 30px;
            }
            .login-form h2 {
                margin: 0 0 15px;
            }
            .form-control, .btn {
                min-height: 38px;
                border-radius: 2px;
            }
            .btn {        
                font-size: 15px;
                font-weight: bold;
            }
        </style>
        </body>
</html>