<?php

include_once '../config.php'; 
  session_start();
    if(!isset($_SESSION['utilisateur'])){
        header('Location:../index.php');
        die();
    }


if (isset($_POST['ordre'])){$_SESSION['ordrematch'] = $_POST['ordre'];}
$ordre_match =$_SESSION['ordrematch'] ;

$nomT = $_SESSION['nomT'];

?>

<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="author" content="NoS1gnal"/>

            <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <title>Connexion</title>
        </head>
        <body>
        <div class="login-form">
            <?php 
                if(isset($_GET['reg_err']))
                {
                    $err = htmlspecialchars($_GET['reg_err']);

                    switch($err)
                    {
                        case 'success':
                        ?>
                            <div class="alert alert-success">
                                <strong>Succès</strong> Les scores de ce match ont bien été inscrits !
                            </div>
                        <?php
                        break;
                   
                        case 'samescore':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Egalité</strong> Les scores sont identiques ! Veuillez entrer deux scores différents !
                            </div>
                        <?php
                       

                    }
                }
                ?>
                  
                <?php
             
                $request_idTournoi = $bdd->prepare('SELECT * FROM tournoi WHERE Nom_Tournoi = ? '); // Recup l'idTournoi via nomT(le nom d'un tournoi')
                $request_idTournoi->execute(array($nomT));
                $idTournoi_data = $request_idTournoi->fetch(); 
                $idTournoi_recup = $idTournoi_data['idTournoi'];  // idTournoi_recup contient bien l'id du tournoi correspondant
                $idTournoi_equipe = $idTournoi_data['Nb_Equipe'];


                //Essayons de trouver le nbr d 'equipes etant déja saisie dans joue
                $request_equipevalide = $bdd->prepare('SELECT * FROM joue, tournoi , tournoi.match WHERE (Nom_Tournoi = ? ) AND (tournoi.idTournoi = match.idTournoi) AND (match.idMatch = joue.idMatch) AND idEquipe1 IS NOT NULL AND idEquipe2 IS NOT NULL '); // Recup l'idTournoi via nomT(le nom d'un tournoi')
                $request_equipevalide->execute(array($nomT));
                $Equipevalide = $request_equipevalide->fetch(); 
                $row = 2 * $request_equipevalide->rowCount();  // row donne bien le nb d'equipe etant deja saisie dans joue

                 //REcuperation de l'idTourmax du tournoi 
                $query_Tour = $bdd->prepare('SELECT MAX(se_compose_de.idTour) FROM se_compose_de WHERE (idTournoi = ? ) ');
                $query_Tour->execute(array($idTournoi_recup));
                $Tour_data = $query_Tour->fetch();
                $idTourmax = $Tour_data['MAX(se_compose_de.idTour)']; // idTourmax donne bien l'idTour max pour un idTournoi donné 
                //echo $idTourmax;

                
                $idTourAct = $_SESSION['idTourAct'];

                //REcuperation de du nom de l'idTourmax : c'est le premier tour !
                $query_nomTour = $bdd->prepare('SELECT Nom_Tour FROM tour WHERE idTour = ?');
                $query_nomTour->execute(array($_SESSION['idTourAct']));
                $nomTour_data = $query_nomTour->fetch();
                $nomTour = $nomTour_data['Nom_Tour']; // nomTourmax donne bien le nom correspondant a idTourmax !
                

                 
                                         

                $email = $_SESSION['utilisateur'];    
                $id = $bdd->prepare('SELECT * FROM utilisateur WHERE ( Email = ? )');
                $id->execute(array($email));
                $id_data = $id->fetch(); 
                $id_gestio = $id_data['idIndividu'];  
                 

                //Recuperons la liste les match par leurs ordres correspondant au match actuel

                $query = $bdd->prepare('SELECT Ordre FROM tournoi.match,joue WHERE (match.idMatch = joue.idMatch) AND (idTour = ? ) AND (match.idTournoi = ? ) ');// Prepare la requete
                $query->execute(array($idTourAct,$idTournoi_recup));  
                $query_data1 = $query->fetchAll(PDO::FETCH_COLUMN, 0);
                                             
                


               /* $query_match= $bdd->prepare('SELECT Ordre FROM joue, tournoi , tournoi.match WHERE (tournoi.Nom_Tournoi = ? ) AND (joue.idTour = ?) AND (tournoi.idTournoi = match.idTournoi) AND (match.idMatch = joue.idMatch) AND idEquipe1 IS NULL AND idEquipe2 IS NULL ');// Prepare la requete
                $query_match->execute(array($nomT,$_SESSION['idTourAct'])); // Execute la requete
                
                $query_matchbis = $query_match->fetchAll(PDO::FETCH_COLUMN, 0);*/
                
                //Recup nomEquipe1 via Ordre
                $query_nome1 = $bdd->prepare('SELECT * FROM joue,tournoi.match,equipe WHERE (Ordre = ?) AND  (match.idTournoi = ?) AND (joue.idMatch = match.idMatch) AND (equipe.idEquipe = joue.idEquipe1)');
                //Recup nomEquipe2 via Ordre
                 $query_nome2 = $bdd->prepare('SELECT * FROM joue,tournoi.match,equipe WHERE (Ordre = ?) AND  (match.idTournoi = ?) AND (joue.idMatch = match.idMatch) AND (equipe.idEquipe = joue.idEquipe2)');
            ?>

                                           
       



            <h4 class="text-center"> TOURNOI </br> <?php echo $nomT  ?> </h4> 
        <p class="text-center"> Le tour actuel: <?php  echo $nomTour ?>  </p> 
         <p class="text-center"> Match n°<?php  echo $ordre_match ?>  </p> 
      <!-- <select class="text-center" name="equipe1"> -->
     <?php
     
           
           $query_nome1->execute(array($ordre_match,$idTournoi_recup));
           $query_nome2->execute(array($ordre_match,$idTournoi_recup));
           $nome1_data = $query_nome1->fetch();
           $nome2_data = $query_nome2->fetch();
           $nome1 = $nome1_data['Nom_Equipe'];
           $nome2 = $nome2_data['Nom_Equipe'];
           $score1 = $nome1_data['Score1'];
           $score2 = $nome2_data['Score2'];

           if ($score1 == NULL)
           {
                  $valscore1 = "Vide";
           }
           else{
                  $valscore1 = $score1;
           }

           if ($score2 == NULL)
           {
                  $valscore2 = "Vide";
           }
           else{
                  $valscore2 = $score2;
           }


           
           
           ?> 
           
           
       <form method="post" action="saisie_scorebis_traitement.php">
        

           <div>
           
           
           <p class="text-center">Equipe 1 - <?php echo $nome1 ?> </p>
           <p class="text-center"> Score : <?php echo $valscore1 ?></p>
           <p class="text-center"> VS </p>
           <p class="text-center">Equipe 2 -<?php echo $nome2 ?> </p>
            <p class="text-center"> Score : <?php echo $valscore2 ?></p>            
           </br>
           <p class="text-center">Entrez les scores de ce match : </p>
           Score 1
           <div> <input class="text-center" type="number" min ='0' name="score1" id="name"></div>
           Score 2
           <div> <input class="text-center" type="number" min ='0' name="score2" id="name" ></div>
           <input id="ordre" name="ordre" type="hidden" value=<?php echo $ordre_match ?>>
           </div>
           <input type="submit" value="check">
        
           
           </form>
           
      
 
                </div>

   

 
            
            <p class="text-center"><a href="saisie_score.php">Retour a la selection de mes tournois</a></p>
            <p class="text-center"><a href="saisie_scorebis.php">Retour a la selection de mes matchs</a></p>
            <p class="text-center"><a href="../accueil.php">Accueil</a></p>
        </div>
 
            
        <style>
            .login-form {
                width: 340px;
                margin: 50px auto;
            }
            .login-form form {
                margin-bottom: 15px;
                background: #f7f7f7;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                padding: 30px;
            }
            .login-form h2 {
                margin: 0 0 15px;
            }
            .form-control, .btn {
                min-height: 38px;
                border-radius: 2px;
            }
            .btn {        
                font-size: 15px;
                font-weight: bold;
            }
        </style>
        </body>
</html>