<?php

include_once '../config.php'; 
  session_start();
    if(!isset($_SESSION['utilisateur'])){
        header('Location:../index.php');
        die();
    }


    if(!empty($_POST['nom_tournoi']))
    {
       
        $nomT = htmlspecialchars($_POST['nom_tournoi']); // nomT a bien été recup
        $newnomT = htmlspecialchars($_POST['newnom-tournoi']); // nomT a bien été recup
   
        $request_idTournoi = $bdd->prepare('SELECT * FROM tournoi WHERE Nom_Tournoi = ? '); // Recup l'idTournoi via nomT(le nom d'un tournoi')
       $request_idTournoi->execute(array($nomT));
        $idTournoi_data = $request_idTournoi->fetch(); 
        $idTournoi_recup = $idTournoi_data['idTournoi'];  // idTournoi_recup contient bien l'id du tournoi correspondant
        

        $query_update_nom = $bdd->prepare('UPDATE tournoi SET Nom_Tournoi = ? WHERE (tournoi.idTournoi = ?) '); // Et on l'affecte au tournoi selectionné precedement (CAS 1)
        $query_update_nom->execute(array($newnomT,$idTournoi_recup));
        
        $_SESSION['nomT'] = $newnomT;
        header('Location:modif_tournoi_gestio_bisbis.php?reg_err=successname');
                                               
                                          die();}

              