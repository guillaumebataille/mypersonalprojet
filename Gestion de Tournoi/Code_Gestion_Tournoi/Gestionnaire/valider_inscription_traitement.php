<?php 
 include_once '../config.php'; 
  session_start();
    if(!isset($_SESSION['utilisateur'])){
        header('Location:../index.php');
        die();
    }
    

   if(!empty($_POST['nom_equipe']))
    {
       
        $nomT = $nomT = $_SESSION['nomT']; ; // nomT a bien été recup
        $nomE = $_POST['nom_equipe']; // nomEquipe a bien été recup
        $valeur = $_POST['valide']; // valeur valide ou refus bien recup
        
   
        $request_idTournoi = $bdd->prepare('SELECT * FROM tournoi WHERE Nom_Tournoi = ? '); // Recup l'idTournoi via nomT(le nom d'un tournoi')
        $request_idTournoi->execute(array($nomT));
        $idTournoi_data = $request_idTournoi->fetch(); 
        $idTournoi_recup = $idTournoi_data['idTournoi'];
        $nbEquipe_recup = $idTournoi_data['Nb_Equipe'];// idTournoi_recup contient bien l'id du tournoi correspondant
        
        $request_idEquipe = $bdd->prepare('SELECT * FROM equipe WHERE Nom_Equipe = ? '); // Recup l'idTournoi via nomT(le nom d'un tournoi')
        $request_idEquipe->execute(array($nomE));
        $idEquipe_data = $request_idEquipe->fetch(); 
        $idEquipe_recup = $idEquipe_data['idEquipe'];  // idEquipe_recup contient bien l'id de l'equipe correspondante
        

        //Essayons de trouver les equipes associé au tournoi WHERE idTournoi = celui qu'on a et valide? = true.
                $request_equipevalide = $bdd->prepare('SELECT * FROM tournoi, est_inscrite, equipe WHERE (Nom_Tournoi = ? ) AND (tournoi.idTournoi = est_inscrite.idTournoi) AND (est_inscrite.idEquipe = equipe.idEquipe) AND (est_inscrite.Valide = true)'); // Recup l'idTournoi via nomT(le nom d'un tournoi')
                $request_equipevalide->execute(array($nomT));
                $Equipevalide = $request_equipevalide->fetch(); 
                $row = $request_equipevalide->rowCount();  // row donne bien le nb d'equipe associé au tournoi etant validé'

               

        if ($valeur == 'true'){

            if ($nbEquipe_recup > $row ){//On doit rendre l'équipe valide via un UPDATE 
                    $query_valide = $bdd->prepare('UPDATE est_inscrite SET Valide = ? WHERE (est_inscrite.idEquipe = ?) AND (est_inscrite.idTournoi = ?) ');
                    $query_valide->execute(array(1,$idEquipe_recup,$idTournoi_recup)); // Cette fonction fonctionne bien, 
                    header('Location:valider_inscription_gestio_bis.php?reg_err=successvalide');
                    die();}
            
            else { // Si le nbr max d'equipe est atteint
                    header('Location:valider_inscription_gestio_bis.php?reg_err=full');
                    die();}}
        else{   // Si on souhaite supprimer l'equipe
                // On doit supprimer l'équipe de la liste des equipes associé au tournoi 
                // DELETE FROM `est_inscrite` WHERE `est_inscrite`.`idEquipe` = 16 AND `est_inscrite`.`idTournoi` = 1
            $query_refus = $bdd->prepare('DELETE FROM est_inscrite WHERE (est_inscrite.idEquipe = ?) AND (est_inscrite.idTournoi = ?) ');
            $query_refus->execute(array($idEquipe_recup,$idTournoi_recup));
            header('Location:valider_inscription_gestio_bis.php?reg_err=successrefus');
            die();
            }
        
    }


