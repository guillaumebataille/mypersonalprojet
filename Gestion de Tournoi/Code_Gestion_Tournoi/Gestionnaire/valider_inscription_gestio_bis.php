<?php

include_once '../config.php'; 
  session_start();
    if(!isset($_SESSION['utilisateur'])){
        header('Location:../index.php');
        die();
    }   
    

if (isset($_POST['nom_tournoi'])){
        $_SESSION['nomT'] = htmlspecialchars($_POST['nom_tournoi']);}

?>

<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="author" content="NoS1gnal"/>

            <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <title>Connexion</title>
        </head>
        <body>
        <div class="login-form">
            <?php 
                if(isset($_GET['reg_err']))
                {
                    $err = htmlspecialchars($_GET['reg_err']);

                    switch($err)
                    {
                        case 'successvalide':
                        ?>
                            <div class="alert alert-success">
                                <strong>Succès</strong> L'équipe est maintenant validée pour le tournoi !
                            </div>
                        <?php
                        break;

                        case 'password':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> mot de passe différent
                            </div>
                        <?php
                        break;

                        case 'email':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> email non valide
                            </div>
                        <?php
                        break;

                        case 'email_length':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> email trop long
                            </div>
                        <?php 
                        break;

                        case 'pseudo_length':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> pseudo trop long
                            </div>
                        <?php 
						break;
                        case 'full':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> Ce tournoi est déja complet
                            </div>
                        <?php 

                    }
                }
                ?>
            


                
                <?php

                $email = $_SESSION['utilisateur'];  
                $nomT = $_SESSION['nomT'];

                $request_idTournoi = $bdd->prepare('SELECT * FROM tournoi WHERE Nom_Tournoi = ? '); // Recup l'idTournoi via nomT(le nom d'un tournoi')
                $request_idTournoi->execute(array($nomT));
                $idTournoi_data = $request_idTournoi->fetch(); 
                $idTournoi_recup = $idTournoi_data['idTournoi'];  // idTournoi_recup contient bien l'id du tournoi correspondant
                $idTournoi_equipe = $idTournoi_data['Nb_Equipe'];


                //Essayons de trouver les equipes associé au tournoi WHERE idTournoi = celui qu'on a et valide? = true.
                $request_equipevalide = $bdd->prepare('SELECT * FROM tournoi, est_inscrite, equipe WHERE (Nom_Tournoi = ? ) AND (tournoi.idTournoi = est_inscrite.idTournoi) AND (est_inscrite.idEquipe = equipe.idEquipe) AND (est_inscrite.Valide = true)'); // Recup l'idTournoi via nomT(le nom d'un tournoi')
                $request_equipevalide->execute(array($nomT));
                $Equipevalide = $request_equipevalide->fetch(); 
                $row = $request_equipevalide->rowCount();  // row donne bien le nb d'equipe associé au tournoi etant validé'

                 
                
                


                $id = $bdd->prepare('SELECT * FROM utilisateur WHERE ( Email = ? )');
                $id->execute(array($email));
                $id_data = $id->fetch(); 
                $id_gestio = $id_data['idIndividu'];  
                 // Recup l'id via ma session/email 


                $query = $bdd->prepare('SELECT Nom_Equipe FROM tournoi, est_inscrite, equipe WHERE (Nom_Tournoi = ? ) AND (tournoi.idTournoi = est_inscrite.idTournoi) AND (est_inscrite.idEquipe = equipe.idEquipe) AND (est_inscrite.Valide = false)');// Prepare la requete
                $query->execute(array($nomT)); // Execute la requete
                //$query_data = $query->fetch(); // Attention !!!! un fetch incrémente le pointeur ! Donc pas de fetch inutile car ça peut delete la premiere valeur
                $query_data_bis = $query->fetchAll(PDO::FETCH_COLUMN, 0); // Via la doc PHP, j'ai vu que fetchall sort un array d'array 
                                                                          // Et via l'option pdo fetch column, je specifie quel colone je target ! Colonne 0 correspont a la 1ere ligne du tab donc nom_equipe
                //$query_data_bis;
                /*$query_data_assoc = $query->fetch(PDO::FETCH_ASSOC); 
                 echo $query_data['Nom_Tournoi'];
                 echo count($query_data);
                 echo count($query_data_bis);
                 var_dump($query_data_bis);*/
            ?>

                                           
       

       <form method="post" action="valider_inscription_traitement.php">
        <p class="text-center"> Vous avez choisi le tournoi </br> <?php echo $nomT  ?> </p> 
        <p class="text-center"> Vous avez </br> <?php echo $row ?> / <?php echo $idTournoi_equipe  ?> equipes enregistrées dans ce tournoi </p>  

       <select name="nom_equipe">
     <?php
      for($i=0;$i<count($query_data_bis);$i++) // compte le nombre de case d'array'
      {
           
           if($query_data_bis[$i] == $_POST['Nom_Equipe'])
                { $selected = ' selected'; }
           else { $selected = ''; }

           ?> <option value="<?php echo $query_data_bis[$i];?>" <?php echo $selected;?>>
      <?php echo $query_data_bis[$i]; ?></option>
           <?php
      }
      
 ?>

   </select>

                <div>
					<input type="radio" id="Valide" name="valide" value="true" checked> 
					<label for="Valide">Valider l'equipe </label>
				</div>
                   <div>
					<input type="radio" id="Refuse" name="valide" value="false">
					<label for="Refuse">Refuser l'equipe </label>
				</div>
   <input type="submit" value="check">
   </form>

            <p class="text-center"><a href="../accueil.php">Accueil</a></p>
        </div>
 
            
        <style>
            .login-form {
                width: 340px;
                margin: 50px auto;
            }
            .login-form form {
                margin-bottom: 15px;
                background: #f7f7f7;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                padding: 30px;
            }
            .login-form h2 {
                margin: 0 0 15px;
            }
            .form-control, .btn {
                min-height: 38px;
                border-radius: 2px;
            }
            .btn {        
                font-size: 15px;
                font-weight: bold;
            }
        </style>
        </body>
</html>