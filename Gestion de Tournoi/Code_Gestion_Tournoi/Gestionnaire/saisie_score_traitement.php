<?php

include_once '../config.php'; 
  session_start();
    if(!isset($_SESSION['utilisateur'])){
        header('Location:../index.php');
        die();
    }

    $nomT =$_POST['nom_tournoi'];

    $_SESSION['nomT'] = htmlspecialchars($_POST['nom_tournoi']);

   
     

                $request_idTournoi = $bdd->prepare('SELECT * FROM tournoi WHERE Nom_Tournoi = ?' ); // Recup l'idTournoi via nomT(le nom d'un tournoi')
                $request_idTournoi->execute(array($nomT));
                $idTournoi_data = $request_idTournoi->fetch(); 
                $idTournoi_recup = $idTournoi_data['idTournoi'];  // idTournoi_recup contient bien l'id du tournoi correspondant
                $idEquipe_recup = $idTournoi_data['Nb_Equipe'];  // idEquipe_recup contient bien l'id du tournoi correspondant
        


                  //REcuperation de l'idTourmax du tournoi 
                $query_Tour = $bdd->prepare('SELECT MAX(se_compose_de.idTour), Actuel FROM se_compose_de WHERE (idTournoi = ? ) ');
                $query_Tour->execute(array($idTournoi_recup));
                $Tour_data = $query_Tour->fetch();
                $idTourmax = $Tour_data['MAX(se_compose_de.idTour)']; // idTourmax donne bien l'idTour max pour un idTournoi donné               
                //echo $idTourmax;
                


                //Recuperation de l'ValActuel'
                $query_ValActuel = $bdd->prepare('SELECT * FROM se_compose_de WHERE (idTour = ? ) AND (idTournoi = ?)');
                $query_ValActuel->execute(array($idTourmax,$idTournoi_recup));
                $idValActuel_data = $query_ValActuel->fetch();
                $ValActuel = $idValActuel_data ['Actuel'];
                //echo $ValActuel;
                

                //Recuperation de l'idTourActuel
                $query_idTourAct= $bdd->prepare('SELECT * FROM se_compose_de WHERE (Actuel = ?) AND (idTournoi = ?)');
                $query_idTourAct->execute(array(true,$idTournoi_recup));
                $idTourAct_data = $query_idTourAct->fetch();
                //var_dump($idTourAct_data);
                $idTourAct = $idTourAct_data['idTour'];
                //echo $idTourAct;

                $_SESSION['idTourAct'] = $idTourAct; 

                //Recuperation du nombre d'équipe au lancement du tournoi
                $query_nbetourmax = $bdd->prepare('SELECT * FROM tour WHERE (idTour = ? ) ');
                $query_nbetourmax->execute(array($idTourmax));
                $nbeTourmax_data = $query_nbetourmax->fetch();
                $nbeTourmax = $nbeTourmax_data['Nb_Equipe'];


                $email = $_SESSION['utilisateur'];    
                $id = $bdd->prepare('SELECT * FROM utilisateur WHERE ( Email = ? )');
                $id->execute(array($email));
                $id_data = $id->fetch(); 
                $id_gestio = $id_data['idIndividu'];  
                 // Recup l'id via ma session/email 

                 
                 
                
                


                //Essayons de trouver les equipes associé au tournoi WHERE idTournoi = celui qu'on a et valide? = true.
                $request_equipevalide = $bdd->prepare( 'SELECT * FROM joue, tournoi.match, tournoi WHERE idEquipe1 IS NOT NULL AND idEquipe2 IS NOT NULL AND (joue.idMatch = match.idMatch) AND (tournoi.idTournoi = match.idTournoi) AND (Nom_Tournoi = ?) '); // Recup l'idTournoi via nomT(le nom d'un tournoi')
                $request_equipevalide->execute(array($nomT));
                $Equipevalide = $request_equipevalide->fetch(); 
                $row = $request_equipevalide->rowCount();  // row donne bien le nb d'equipe associé au tournoi etant validé'
                //echo $idEquipe_recup;
                //echo $row;

                if($idEquipe_recup > $row * 2) { // Si y'a pas toute les equipes

                header('Location:saisie_score.php?reg_err=manqueequipe');
                    die();} 
                else{

                //Recuperation du nbr de match necessaire (nbematchtourAct / 2) pour le tour actuel et du nbr de match initialisé pour ce tour 
                $query_nbematchok = $bdd->prepare('SELECT * FROM joue, tour, tournoi.match WHERE (match.idMatch = joue.idMatch) AND (joue.idTour = tour.idTour) AND (joue.idTour = ? ) AND (match.idTournoi = ?) AND idEquipe1 IS NOT NULL AND idEquipe2 IS NOT NULL');
                $query_nbematchok->execute(array($idTourAct,$idTournoi_recup));
                $nbematchok_data = $query_nbematchok->fetch(); 
                $nbematchtourAct = $nbematchok_data['Nb_Equipe'];
                $rowmatch = $query_nbematchok->rowCount();

               // echo $rowmatch; 
               //  echo $nbematchtourAct;
                if($nbematchtourAct/2 == $rowmatch){
                header('Location:saisie_scorebis.php');
                die();} 
                
                else{

                header('Location:saisie_score.php?reg_err=notinit');
                die();} 
                
                
                }
                
              
                
                


 
       