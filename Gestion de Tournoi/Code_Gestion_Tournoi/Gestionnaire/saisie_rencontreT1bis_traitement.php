<?php

include_once '../config.php'; 
  session_start();
    if(!isset($_SESSION['utilisateur'])){
        header('Location:../index.php');
        die();
    }

    $nomT = $_SESSION['nomT'];
    $nomE1 = $_POST['equipe1'];
    $nomE2 = $_POST['equipe2'];
    $date =$_POST['date_horaire'];
    $numMatch =$_POST['num_match'];

   

                //Recup l'idEquipe a partir du nom'    

                $query_idEquipe = $bdd->prepare('SELECT * FROM equipe WHERE ( Nom_Equipe = ? )');
                // Pour equipe1
                $query_idEquipe->execute(array($nomE1));
                $idEquipe_data = $query_idEquipe->fetch(); 
                $idE1= $idEquipe_data['idEquipe'];  
                // Pour equipe2
                $query_idEquipe->execute(array($nomE2));
                $idEquipe_data = $query_idEquipe->fetch(); 
                $idE2= $idEquipe_data['idEquipe']; 
                
                
                // Normalement impossible d'avoir plus d'équipe à fournir
               
               if($idE1 != $idE2 ) // Si les deux equipes sont différentes
               {
                    //On recup l'idMatch correspondant au tournoi - match - joue avec l'ordre adequat et le tournoi correspondant (Ordre et Nom_Tournoi):

                    $query_idMatch = $bdd->prepare('SELECT * FROM tournoi, tournoi.match, joue WHERE (tournoi.idTournoi = match.idTournoi) AND (match.idMatch = joue.idMatch) AND (match.Ordre = ?) AND (Nom_Tournoi = ?)');
                    $query_idMatch->execute(array($numMatch,$nomT));
                    $idMatch_data = $query_idMatch->fetch();
                    $idMatch = $idMatch_data['idMatch'];
                    

                    //On insere la datea l'id du match via update 
                    $query_dateinsert = $bdd->prepare('UPDATE tournoi.match SET Date_Horaire = ? WHERE (idMatch = ?)');
                    $query_dateinsert->execute(array($date,$idMatch));

                    //On insere l'equipe 1 et 2 dans joue - match avec l'idMatch recupéré precedement :
                    $query_einsert = $bdd->prepare('UPDATE joue, tournoi.match SET idEquipe1 = ? , idEquipe2 = ? WHERE (joue.idMatch = match.idMatch) AND (joue.idMatch = ?)');
                    $query_einsert->execute(array($idE1,$idE2,$idMatch));

                    
                    header('Location:saisie_rencontreT1bis.php?reg_err=success');
                    die();


               }
               else { // Si les deux equipes sont identiques on quitte avec msg d'erreur 
                header('Location:saisie_rencontreT1bis.php?reg_err=sameequipe');
                die();
               }



?>
