<?php

include_once '../config.php'; 
  session_start();
    if(!isset($_SESSION['utilisateur'])){
        header('Location:../index.php');
        die();
    }

    $nomT =$_POST['nom_tournoi'];
    
    
    
                $request_idTournoi = $bdd->prepare('SELECT * FROM tournoi WHERE Nom_Tournoi = ? '); 
                $request_idTournoi->execute(array($nomT));
                $idTournoi_data = $request_idTournoi->fetch(); 
                $idTournoi_recup = $idTournoi_data['idTournoi'];  //Recuperation de l'idTournoi via le nomT

               
                $query_idTourAct= $bdd->prepare('SELECT * FROM se_compose_de WHERE (Actuel = ?) AND (idTournoi = ?)');
                $query_idTourAct->execute(array(true,$idTournoi_recup));
                $idTourAct_data = $query_idTourAct->fetch();               
                $idTourAct = $idTourAct_data['idTour'];  //Recuperation de l'idTourActuel via l'idTournoi'


                $query_Ordre_Max = $bdd->prepare('SELECT MAX(Ordre) FROM tournoi.match, joue WHERE (match.idMatch = joue.idMatch) AND (idTournoi = ?) AND (idTour = ?)');
                $query_Ordre_Max->execute(array($idTournoi_recup,$idTourAct));
                $Ordre_Max_data = $query_Ordre_Max->fetch();
                $ordre_Max = $Ordre_Max_data['MAX(Ordre)'];  //Recuperation de l'ordreMax du tour actuel (le dernier match ayant eu lieu)
              

                $query_Ordre_Min= $bdd->prepare('SELECT MIN(Ordre) FROM tournoi.match, joue WHERE (match.idMatch = joue.idMatch) AND (idTournoi = ?) AND (idTour = ?)');
                $query_Ordre_Min->execute(array($idTournoi_recup,$idTourAct));
                $Ordre_Min_data = $query_Ordre_Min->fetch();
                $ordre_Min = $Ordre_Min_data['MIN(Ordre)'];  //Recuperation de l'ordreMax du tour actuel (le dernier match ayant eu lieu)
                
                $query_nbmAct= $bdd->prepare('SELECT COUNT(*) FROM tournoi.match, joue WHERE (match.idMatch = joue.idMatch) AND (idTournoi = ?) AND (idTour = ?)');
                $query_nbmAct->execute(array($idTournoi_recup,$idTourAct));
                $nbmAct_data = $query_nbmAct->fetch();
                $nbmAct = $nbmAct_data['COUNT(*)']; //Recuperation du nbr de match dans le tour actuel
                
                $nbmMatchSuiv = $nbmAct / 2;  // Le nombre de match au tour suivant
                               
                $query_Matchvalide= $bdd->prepare('SELECT COUNT(*) FROM tournoi.match, joue WHERE (match.idMatch = joue.idMatch) AND (idTournoi = ?) AND (idTour = ?) AND Score1 IS NOT NULL AND Score2 IS NOT NULL');
                $query_Matchvalide->execute(array($idTournoi_recup,$idTourAct));
                $Matchvalide_data = $query_Matchvalide->fetch();
                $matchvalide = $Matchvalide_data['COUNT(*)']; // Le nombre de match ayant été joué (avec un score attribué des deux cotés)
 

                $ordresuiv = $ordre_Max; // L'ordre qui va evoluer pour les matchs suivant
                $idToursuiv = $idTourAct - 1; // Le tour suivant

                if ($nbmAct == $matchvalide) {

                        if ($idTourAct > 3){

                                while($ordre_Min < $ordre_Max){

                                // Partie 1 
                                $query_M_Act= $bdd->prepare('SELECT * FROM tournoi.match, joue WHERE (match.idMatch = joue.idMatch) AND (idTournoi = ?) AND (idTour = ?) AND (Ordre = ?) ');
                                $query_M_Act->execute(array($idTournoi_recup,$idTourAct,$ordre_Min));
                                $M_Act_data = $query_M_Act->fetch();
                                $idM_Act = $M_Act_data['idMatch'];  
                                $score1 = $M_Act_data['Score1'];
                                $score2 = $M_Act_data['Score2'];
                                $idE1 = $M_Act_data['idEquipe1'];
                                $idE2 = $M_Act_data['idEquipe2']; //Recuperation des données relatives au match actuellement etudié (de l'ordre min au max du tourAct)

                                if ($score1 > $score2) // On determine le 1er gagnant dan idwin1
                                {
                                    $idwin1 = $idE1;  
                                }else{
                                    $idwin1 = $idE2; 
                                } 

                                $ordre_Min = $ordre_Min + 1; // Match suivant du tour actuel 

                                // Partie 2
                                $query_M_Act= $bdd->prepare('SELECT * FROM tournoi.match, joue WHERE (match.idMatch = joue.idMatch) AND (idTournoi = ?) AND (idTour = ?) AND (Ordre = ?) ');
                                $query_M_Act->execute(array($idTournoi_recup,$idTourAct,$ordre_Min));
                                $M_Act_data = $query_M_Act->fetch();
                                $idM_Act = $M_Act_data['idMatch'];  
                                $score1 = $M_Act_data['Score1'];
                                $score2 = $M_Act_data['Score2'];
                                $idE1 = $M_Act_data['idEquipe1'];
                                $idE2 = $M_Act_data['idEquipe2']; //Recuperation des données relatives au match actuellement etudié (de l'ordre min au max du tourAct)

                                if ($score1 > $score2) // On determine le 2eme gagnant dan idwin2
                                {
                                    $idwin2 = $idE1;  
                                }else{
                                    $idwin2 = $idE2; 
                                } 

                                $ordre_Min = $ordre_Min + 1;

                                // Partie 3 
                                $ordresuiv = $ordresuiv + 1; //Match suivant du tour suivant

                                $query_Msuiv= $bdd->prepare('SELECT * FROM tournoi.match, joue WHERE (match.idMatch = joue.idMatch) AND (idTournoi = ?) AND (idTour = ?) AND (Ordre = ?) ');
                                $query_Msuiv->execute(array($idTournoi_recup,$idToursuiv,$ordresuiv));
                                $Msuiv_data = $query_Msuiv->fetch();
                                $idM_suiv = $Msuiv_data['idMatch'];  

                                $query_insertscore = $bdd->prepare('UPDATE tournoi.match, joue SET  idEquipe1 = ?, idEquipe2 = ? WHERE (joue.idMatch = ?) AND (match.idMatch = joue.idMatch)  ');
                                $query_insertscore->execute(array($idwin1,$idwin2,$idM_suiv));
                 
                                } //endwhile

                        $query_updateTourold = $bdd->prepare('UPDATE se_compose_de SET Actuel = ? WHERE (idTournoi = ?) AND (idTour = ?)  ');
                        $query_updateTourold->execute(array(0,$idTournoi_recup,$idTourAct)); // On retire le tour précédent comme tour actuel
                
                        $query_updateTournew = $bdd->prepare('UPDATE se_compose_de SET Actuel = ? WHERE (idTournoi = ?) AND (idTour = ?)  ');
                        $query_updateTournew->execute(array(1,$idTournoi_recup,$idToursuiv)); // On definit le nouveau tour comme tour actuel

                        header('Location:tour_suivant.php?reg_err=success');
                        die();}// endif ordre > 3 
                
                else 
                
                if ($idTourAct == 3) {  // CAS SPECIAL DE LA DEMI-FINALE 


                        $query_M_Act= $bdd->prepare('SELECT * FROM tournoi.match, joue WHERE (match.idMatch = joue.idMatch) AND (idTournoi = ?) AND (idTour = ?) AND (Ordre = ?) ');
                        $query_M_Act->execute(array($idTournoi_recup,$idTourAct,$ordre_Min));
                        $M_Act_data = $query_M_Act->fetch();
                        $idM_Act = $M_Act_data['idMatch'];  
                        $score1 = $M_Act_data['Score1'];
                        $score2 = $M_Act_data['Score2'];
                        $idE1 = $M_Act_data['idEquipe1'];
                        $idE2 = $M_Act_data['idEquipe2']; //Recuperation des données relatives au match actuellement etudié (de l'ordre min au max du tourAct)

                        if ($score1 > $score2) // On determine le 1er gagnant dan idwin1 et le perdant (CAR CAR SPECIAL DEMI FINALE)
                        {
                            $idwin1 = $idE1;
                            $idlose1 = $idE2;
                        }else{
                            $idwin1 = $idE2; 
                            $idlose1 = $idE1;
                        }   

                        $ordre_Min = $ordre_Min + 1; // Match suivant du tour actuel 

                        // Partie 2
                        $query_M_Act= $bdd->prepare('SELECT * FROM tournoi.match, joue WHERE (match.idMatch = joue.idMatch) AND (idTournoi = ?) AND (idTour = ?) AND (Ordre = ?) ');
                        $query_M_Act->execute(array($idTournoi_recup,$idTourAct,$ordre_Min));
                        $M_Act_data = $query_M_Act->fetch();
                        $idM_Act = $M_Act_data['idMatch'];  
                        $score1 = $M_Act_data['Score1'];
                        $score2 = $M_Act_data['Score2'];
                        $idE1 = $M_Act_data['idEquipe1'];
                        $idE2 = $M_Act_data['idEquipe2']; //Recuperation des données relatives au match actuellement etudié (de l'ordre min au max du tourAct)

                        if ($score1 > $score2) // On determine le 2eme gagnant dans idwin2 et le perdant (CAR CAS SPECIAL DEMI FINALE)
                        {
                            $idwin2 = $idE1;  
                            $idlose2 = $idE2;
                        }else{
                            $idwin2 = $idE2;
                            $idlose2 = $idE1;
                        } 
                
                        // On a les id des gagnants (FINALE) et des perdant (Petite-finale), il suffit d'implémenter   
                        //Petite-finale
                        $query_insertscore = $bdd->prepare('UPDATE tournoi.match, joue SET  idEquipe1 = ?, idEquipe2 = ? WHERE (idTour = ?) AND (match.idTournoi = ?) AND (match.idMatch = joue.idMatch)  ');
                        $query_insertscore->execute(array($idlose1,$idlose2,2,$idTournoi_recup));
                        //finale
                        $query_insertscore = $bdd->prepare('UPDATE tournoi.match, joue SET  idEquipe1 = ?, idEquipe2 = ? WHERE (idTour = ?) AND (match.idTournoi = ?) AND (match.idMatch = joue.idMatch)  ');
                        $query_insertscore->execute(array($idwin1,$idwin2,1,$idTournoi_recup));

                       
                        $query_updateTourold = $bdd->prepare('UPDATE se_compose_de SET Actuel = ? WHERE (idTournoi = ?) AND (idTour = ?)  ');
                        $query_updateTourold->execute(array(0,$idTournoi_recup,$idTourAct)); // On retire le tour précédent comme tour actuel
                
                        $query_updateTournew = $bdd->prepare('UPDATE se_compose_de SET Actuel = ? WHERE (idTournoi = ?) AND (idTour = ?)  ');
                        $query_updateTournew->execute(array(1,$idTournoi_recup,$idToursuiv)); // On definit le nouveau tour comme tour actuel
 
                
                        header('Location:tour_suivant.php?reg_err=successdemi');
                        die();} //endif idTourAct = demi
                
                else 
                
                if ($idTourAct == 2) {

                        $query_updateTourold = $bdd->prepare('UPDATE se_compose_de SET Actuel = ? WHERE (idTournoi = ?) AND (idTour = ?)  ');
                        $query_updateTourold->execute(array(0,$idTournoi_recup,$idTourAct)); // On retire le tour précédent comme tour actuel
                
                        $query_updateTournew = $bdd->prepare('UPDATE se_compose_de SET Actuel = ? WHERE (idTournoi = ?) AND (idTour = ?)  ');
                        $query_updateTournew->execute(array(1,$idTournoi_recup,$idToursuiv)); // On definit le nouveau tour comme tour actuel
                        header('Location:tour_suivant.php?reg_err=successversfinale');
                        die();} 
                
                else 
                
                if ($idTourAct == 1) {


                        //$query_updateTourold = $bdd->prepare('UPDATE se_compose_de SET Actuel = ? WHERE (idTournoi = ?) AND (idTour = ?)  ');
                        //$query_updateTourold->execute(array(0,$idTournoi_recup,$idTourAct)); // On retire le tour précédent comme tour actuel
                        header('Location:tour_suivant.php?reg_err=fin');
                        die();}
                


                } // endif si y'a des score a NULL
                else{
                header('Location:tour_suivant.php?reg_err=scoreerror');
                die();} 

                