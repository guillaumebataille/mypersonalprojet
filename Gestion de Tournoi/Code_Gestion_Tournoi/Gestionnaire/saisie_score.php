<?php

include_once '../config.php'; 
  session_start();
    if(!isset($_SESSION['utilisateur'])){
        header('Location:../index.php');
        die();
    }
?>

<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="author" content="NoS1gnal"/>

            <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <title>Connexion</title>
        </head>
        <body>
        <div class="login-form">
           <?php 
                if(isset($_GET['reg_err']))
                {
                    $err = htmlspecialchars($_GET['reg_err']);

                    switch($err)
                    {
                        case 'success':
                        ?>
                            <div class="alert alert-success">
                                <strong>Succès</strong> inscription réussie !
                            </div>
                        <?php
                        break;

                        case 'notfull':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> Le tournoi n'est pas complet, veuillez le remplir'
                            </div>
                        <?php
                        break;

                        case 'notinit':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> Le tournoi n'est pas initialisé, veuillez l'initialiser pour le premier tour
                            </div>
                        <?php
                        break;

                        case 'email_length':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> email trop long
                            </div>
                        <?php 
                        break;

                        case 'pseudo_length':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> pseudo trop long
                            </div>
                        <?php 
						break;
                        case 'already':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> Adresse email deja used
                            </div>
                        <?php 

                    }
                }
                ?>

               
                <?php

                $email = $_SESSION['utilisateur'];    
                $id = $bdd->prepare('SELECT * FROM utilisateur WHERE ( Email = ? )');
                $id->execute(array($email));
                $id_data = $id->fetch(); 
                $id_gestio = $id_data['idIndividu'];  
                 // Recup l'id via ma session/email 


                $query = $bdd->prepare("SELECT * FROM tournoi, organise WHERE (organise.idTournoi = tournoi.idTournoi ) AND (idIndividuGestio = ?) "); // Prepare la requete
                $query->execute(array($id_gestio)); // Execute la requete
                //$query_data = $query->fetch(); // Attention !!!! un fetch incrémente le pointeur ! Donc pas de fetch inutile car ça peut delete la premiere valeur
                $query_data_bis = $query->fetchAll(PDO::FETCH_COLUMN, 1); // Via la doc PHP, j'ai vu que fetchall sort un array d'array
                                                                          // Et via l'option pdo fetch column, je specifie quel colone je target ! (5h)'
               
                /*$query_data_assoc = $query->fetch(PDO::FETCH_ASSOC); 
                 echo $query_data['Nom_Tournoi'];
                 echo count($query_data);
                 echo count($query_data_bis);
                 var_dump($query_data_bis);*/
            ?>

                                           
        <h2 class="text-center"> Veuillez choisir parmis vos tournois ! </h2> 

       <form method="post" action="saisie_score_traitement.php">
       
       <select name="nom_tournoi">
     <?php
      for($i=0;$i<count($query_data_bis);$i++) // compte le nombre de case d'array'
      {
           
           if($query_data_bis[$i] == $_POST['Nom_Tournoi'])
                { $selected = ' selected'; }
           else { $selected = ''; }

           ?> <option value="<?php echo $query_data_bis[$i];?>" <?php echo $selected;?>>
      <?php echo $query_data_bis[$i]; ?></option>
           <?php
      }
      //echo $query_data_bis[1];
 ?>
   </select>
   <input type="submit" value="check">
   </form>

  


    

 
            <p class="text-center"><a href="../accueil.php">Accueil</a></p>
        </div>
 
            
        <style>
            .login-form {
                width: 340px;
                margin: 50px auto;
            }
            .login-form form {
                margin-bottom: 15px;
                background: #f7f7f7;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                padding: 30px;
            }
            .login-form h2 {
                margin: 0 0 15px;
            }
            .form-control, .btn {
                min-height: 38px;
                border-radius: 2px;
            }
            .btn {        
                font-size: 15px;
                font-weight: bold;
            }
        </style>
        </body>
</html>