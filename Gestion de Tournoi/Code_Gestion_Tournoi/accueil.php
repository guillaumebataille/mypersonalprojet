<?php
require_once 'config.php';
session_start();
if (!isset($_SESSION['utilisateur'])) {
    header('Location:index.php');
    die();
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Chill&Tea - Accueil</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Logo -->
    <link rel="icon" href="img/logo.png">
    <link rel="apple-touch-icon" href="img/logo.png">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>


<?php

$email = $_SESSION['utilisateur'];
$role = $bdd->prepare('SELECT * FROM utilisateur, role, attributionrole WHERE ( utilisateur.idIndividu = attributionrole.idIndividu ) AND ( attributionrole.idRole = role.idRoleR ) AND ( utilisateur.Email = ? ) ORDER BY idRoleR');
$roleisOk = $role->execute(array($email));
$rolerecup = $role->fetch();
$roleupdate = $rolerecup['Nom_Role'];
$loginupdate = $rolerecup['Login'];


?>


<div id="h">
    <div class="parallax"></div>
    <!-- Menu de navigation -->

    <img class="logopng" src="img/logo.png" alt="Logo Chill&Tea">
    <div class="logo"><a href="">Chill&Tea</a></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 centered">


                <h1>Bienvenue sur Chill&Tea.<br/>Vous êtes connecté : <?php echo $loginupdate; ?> ! <br/> Vous êtes
                    actuellement un : <?php echo $roleupdate; ?> </br>
                    <?php if ($roleupdate === 'Admin')
                        echo "Voici vos droits !"
                    ?>

                    <?php if ($roleupdate === 'Gestionnaire')
                        echo "Voici vos droits !"
                    ?>

                    <?php if ($roleupdate === 'Capitaine')
                        echo "Voici vos droits !"
                    ?>

                </h1>
                <div class="mtb">

                    <?php if ($roleupdate === 'Admin')
                        echo '<p class="text-center">' . '<a href="/website/Admin/creer_tournoi.php" class="btn btn-conf btn-green">' . 'Creer un tournoi' . '<a href="/website/Admin/Affectation_Gestio.php" class="btn btn-conf btn-green">' . 'Affecter un Gestionnaire' . '</a>' . '<a href="/website/Admin/inscriptionadmin.php" class="btn btn-conf btn-green">' . 'Inscrire un Gestio/Admin/Capitaine' . '</a>' . '</p>'
                    ?>

                    <?php if ($roleupdate === 'Gestionnaire')
                        echo '<p class="text-center">' . '<a href="/website/Gestionnaire/modif_tournoi_gestio.php" class="btn btn-conf btn-green">' . 'Modifier un tournoi' . '<a href="/website/Gestionnaire/valider_inscription_gestio.php" class="btn btn-conf btn-green">' . 'Valider les inscriptions en attente' . '</a>' . '<a href="/website/Gestionnaire/saisie_rencontreT1.php" class="btn btn-conf btn-green">' . 'Saisir les rencontres initiales' . '<a href="/website/Gestionnaire/saisie_score.php" class="btn btn-conf btn-green">' . 'Saisir un score' . '</a>' . '</p>'
                    ?>

                    <?php if ($roleupdate === 'Capitaine') {
                        $result = $bdd->prepare('SELECT * FROM Equipe WHERE idIndividuCapitaine = ?');
                        $result->execute(array($rolerecup['idIndividu']));
                        $data = $result->fetch();
                        $count = $result->rowCount();
                        echo '<p class="text-center">';
                        if ($count > 0) {
                            echo '<a href="/website/Team/mon_equipe.php" class="btn btn-conf btn-green">' . 'Mon equipe' . '</a>';
                            echo '<a data-toggle="modal" data-target="#create_player" class="btn btn-conf btn-green">' . 'Créer les joueurs' . '</a>';
                        } else {
                            echo '<a data-toggle="modal" data-target="#create_team" class="btn btn-conf btn-green">' . 'Créer une équipe' . '</a>';
                        }
                        echo '</p>';
                    }

                    ?>

                </div>
                <!--/mt-->
                <h6>Service gratuit pour les particuliers et les professionnels.</h6>
                <div class="text-center">

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal"
                            data-target="#change_password">
                        Changer mon mot de passe
                    </button>
                </div>
            </div>
        </div>
        <!--/row-->
    </div>
    <!--/container-->
    <div class="log"><a href="connexion/deconnexion_traitement.php">Déconnexion</a></div>

</div>
<!-- /H -->

<div class="container ptb">
    <div class="row">
        <div class="col-md-6">
            <h2 class="h2">Créer. Gérer. Gagner !</h2>
            <p class="mt">Nos administrateurs utilisent les outils de créations de tournois et ont a disposition un
                large choix d'options pour créer des compétitions uniques. <br>
                Les gestionnaires de tournois quant à eux ont accès a de nombreuses options, y compris une page dédiée,
                pour pouvoir mettre à jour les compétitions.</p>
        </div>
        <div class="col-md-6">
            <div class="cup">
                <img src="img/cup.png" class="img-responsive mt" alt="Coupe">
            </div>
        </div>
    </div>
    <!--/row-->
</div>
<!--/container-->

<div id="sep">
    <div class="container">
        <div class="row centered">
            <div class="col-md-8 col-md-offset-2">

                <h1>Consulter tout les tournois Chill&Tea !</h1>

                <h4>Découvre les résultats et les rencontres des tournois passés, en cours et à venir. Clique sur le
                    bouton ci-dessous pour accéder à la page d'affichage des tournois.</h4>
                <p>


                    
                <?php if ($roleupdate === 'Capitaine' AND $count > 0){           
                        echo '<a href="Tournoi/tournoiscapitaine.php" class="btn btn-conf btn-green">' . 'Tournois' . '</a>';}
                        else {echo '<a href="Tournoi/tournois.php" class="btn btn-conf btn-green">' . 'Tournois' . '</a>';
                    }?>
                    
                    
                </p>

            </div>
            <!--/col-md-8-->
        </div>
    </div>
</div>
<!--/sep-->

<div id="g">
    <div class="container">
        <div class="row sponsor centered">
            <div class="col-sm-2 col-sm-offset-1">
                <img src="img/client1.png" alt="">
            </div>
            <div class="col-sm-2">
                <img src="img/client3.png" alt="">
            </div>
            <div class="col-sm-2">
                <img src="img/client2.png" alt="">
            </div>
            <div class="col-sm-2">
                <img src="img/client4.png" alt="">
            </div>
            <div class="col-sm-2">
                <img src="img/client5.jpg" alt="">
            </div>
        </div>
        <!--/row-->
    </div>
</div>
<!--/g-->

<div id="f">
    <div class="container">
        <div class="row centered">
            <h2>Nous contacter !</h2>
            <h5>CHILLANDTEA@OUTLOOK.FR</h5>
            <a href="mailto:chillandtea@outlook.fr"><img class="mail" src="img/mail.png" alt="mail"></a>
        </div>
    </div>
    <!--/container-->
</div>
<!--/F-->

<div id="copyrights">
    <div class="container">
        <p>
            &copy; Copyrights <strong>Chill&Tea</strong>. Tous droits réservés.
        </p>
        <div class="credits">
            <!-- Ne pas supprimer pour raison de droits d'auteurs ! -->

            <!-- =======================================================
            Template Name: SumoLanding
            Template URL: https://templatemag.com/sumolanding-bootstrap-landing-template/
            Author: TemplateMag.com
            License: https://templatemag.com/license/
            ======================================================= -->
            Créé avec la template SumoLanding par <a href="https://templatemag.com/">TemplateMag</a>
            <br/>
            Notre logo provient de <a href='https://pngtree.com/so/cute-vector'>PngTree</a> !

        </div>
    </div>
</div>
<!-- / copyrights -->

<!-- JavaScript Libraries -->
<script src="lib/jquery/jquery.min.js"></script>
<script src="lib/bootstrap/js/bootstrap.min.js"></script>
<script src="lib/php-mail-form/validate.js"></script>

<!-- Template Main Javascript File -->
<script src="js/main.js"></script>


<div class="container">
    <div class="col-md-12">
        <?php
        if (isset($_GET['err'])) {
            $err = htmlspecialchars($_GET['err']);
            switch ($err) {
                case 'current_password':
                    echo "<div class='alert alert-danger'>Le mot de passe actuel est incorrect</div>";
                    break;

                case 'success_password':
                    echo "<div class='alert alert-success'>Le mot de passe a bien été modifié ! </div>";
                    break;
            }
        }
        ?>


    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="change_password" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Changer mon mot de passe</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="layouts/change_password.php" method="POST">
                    <label for='current_password'>Mot de passe actuel</label>
                    <input type="password" id="current_password" name="current_password" class="form-control" required/>
                    <br/>
                    <label for='new_password'>Nouveau mot de passe</label>
                    <input type="password" id="new_password" name="new_password" class="form-control" required/>
                    <br/>
                    <label for='new_password_retype'>Re tapez le nouveau mot de passe</label>
                    <input type="password" id="new_password_retype" name="new_password_retype" class="form-control"
                           required/>
                    <br/>
                    <button type="submit" class="btn btn-success">Sauvegarder</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="avatar" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Changer mon avatar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="layouts/change_avatar.php" method="POST" enctype="multipart/form-data">
                    <label for="avatar">Images autorisées : png, jpg, jpeg, gif - max 20Mo</label>
                    <input type="file" name="avatar_file">
                    <br/>
                    <button type="submit" class="btn btn-success">Modifier</button>
                </form>
            </div>
            <br/>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>


<?php

if (isset($_POST['create_team']) && !empty($_POST['name_team']) && !empty($_POST['phone']) && !empty($_POST['level']) && !empty($_POST['address']) && !empty($_POST['city'])) {
    $name_team = htmlspecialchars($_POST['name_team']);
    $phone = htmlspecialchars($_POST['phone']);
    $level = htmlspecialchars($_POST['level']);
    $address = htmlspecialchars($_POST['address']);
    $city = htmlspecialchars($_POST['city']);


    $insert = $bdd->prepare('INSERT INTO equipe (Nom_Equipe, Niveau_Equipe, Ville, Adresse, Num_Telephone, idIndividuCapitaine) VALUES(:name, :level, :city, :address, :phone, :leader_id)');
    $insert->execute(array(
        'name' => $name_team,
        'level' => $level,
        'city' => $phone,
        'address' => $address,
        'phone' => $address,
        'leader_id' => $rolerecup['idIndividu']
    ));


//    $last_id = $bdd->lastInsertId();
//    $teamId = $last_id;
//    $insert = $bdd->prepare('INSERT INTO person_team (person_id, team_id, isLead)VALUES(:person_id, :team_id, :isLead)');
//    $insert->execute(array(
//        'person_id' => $personId,
//        'team_id' => $last_id,
//        'isLead' => 1
//    ));

    echo "<meta http-equiv='refresh' content='0'>";
}

?>

<!-- Modal Create Team -->
<div class="modal fade" id="create_team" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Création de l'équipe</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="POST">
                    <div class="form-group">
                        <label>Nom de l'équipe</label>
                        <input type="text" id="name_team" name="name_team" class="form-control" required/>
                    </div>

                    <div class="form-group">
                        <label>Téléphone</label>
                        <input type="text" id="phone" name="phone" class="form-control" required/>
                    </div>

                    <div class="form-group">
                        <label>Niveau de l'équipe</label>
                        <input type="number" id="level" name="level" class="form-control" required/>
                    </div>

                    <div class="form-group">
                        <label>Adresse</label>
                        <input type="text" id="address" name="address" class="form-control" required/>
                    </div>

                    <div class="form-group">
                        <label>Ville</label>
                        <input type="text" id="city" name="city" class="form-control" required/>
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-success" name="create_team">Enregistrer</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


<?php
echo $data['idEquipe'];
if (isset($_POST['create_player']) && !empty($_POST['gender']) && !empty($_POST['lastname']) && !empty($_POST['firstname'])) {
    $gender = htmlspecialchars($_POST['gender']);
    $lastname = htmlspecialchars($_POST['lastname']);
    $firstname = htmlspecialchars($_POST['firstname']);

    $insert = $bdd->prepare('INSERT INTO utilisateur (Genre, Nom, Prenom) VALUES(:gender, :lastname, :firstname)');
    $insert->execute(array(
        'gender' => $gender,
        'firstname' => $lastname,
        'lastname' => $firstname,
    ));

    $lastId = $bdd->lastInsertId();

    echo $lastId;


    $insert = $bdd->prepare('INSERT INTO est_membre_de (idJoueur, idEquipe)VALUES(:person_id, :team_id)');
    $insert->execute(array(
        'person_id' => $lastId,
        'team_id' => $data['idEquipe']
    ));
}
?>

<!-- Modal Create Player-->
<div class="modal fade" id="create_player" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Créer les joueurs</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="POST">
                    <div>
                        <input type="radio" name="gender" value="Homme" checked>
                        <label>Homme</label>

                        <input type="radio" name="gender" value="Femme">
                        <label>Femme</label>
                    </div>
                    <!-- NOM  -->
                    <div class="form-group">
                        <input type="text" name="lastname" class="form-control" placeholder="Nom" required="required">
                    </div>

                    <!--  PRENOM -->
                    <div class="form-group">
                        <input type="text" name="firstname" class="form-control" placeholder="Prenom" required="required">
                    </div>

                    <br/>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-success" name="create_player">Enregistrer</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>


</body>
</html>